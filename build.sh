#!/bin/bash


cp dist/build/antibot/antibot antibot-1.0/
cp -R src/static antibot-1.0/
cp dist/build/log-to-udp/log-to-udp log-to-udp-1.0/

cd antibot-1.0
dpkg-buildpackage

cd ../log-to-udp-1.0
dpkg-buildpackage

