#!/bin/sh

ANTIBOTUSER=antibot
export ANTIBOTUSER

ANTIBOTGROUP=antibot
export ANTIBOTGROUP

ANTIBOTWWWPORT=8000
export ANTIBOTWWWPORT

ANTIBOTUDPPORT=5001
export ANTIBOTUDPPORT

ANTIBOTSTATIC=/var/www/antibot
export ANTIBOTSTATIC

ANTIBOTCONFPATH=/opt/antibot
export ANTIBOTCONFPATH

DAEMON=/opt/antibot/antibot


case "$1" in
    start)
		echo start
        $DAEMON start
         ;;
    stop)
        $DAEMON stop
        ;;
    restart)
        $DAEMON restart
        ;;
    *)
        echo "Usage: /etc/init.d/antibot start|stop|restart"
        exit 1
        ;;
        
esac
exit 0
