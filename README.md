Antibot
=======

Предназначение
==============

Антибот для отсева ботов по нескольким параметрам:

1. Количество запросов в единицу времени по определенным роутам.
2. Совпадение строки с регулярными выражениями в полях ввода.
3. Отсутствие запросов по определенным роутам.

Описание
========

Состоит из двух приложений.

log-to-udp
==========

Простое приложение, читает access_log веб сервера, сериализует в бинарный поток, шлет поток по udp на antibot.

Формат лога на примере nginx:

    log_format  myformat '$remote_addr - $remote_user [$time_local] $request_time '
        '"$request" $status $body_bytes_sent '
        '"$http_referer" "$http_user_agent" "$host"';

При старте читает конфигурационный файл, cледущего формата:

    [antibot.example.org]
    file: /var/log/nginx/access_log
    port: 7000

    #[antibot1.example.org]
    #file: /var/log/nginx/access_log
    #port: 6001

Также для предварительной настройки используются переменные окружения:

    LOGTOUDPUSER=www-data # - user от которого будет работать демон
    export LOGTOUDPUSER
    
    LOGTOUDPGROUP=www-data #- аналогично для группы
    export LOGTOUDPGROUP
    
    LOGTOUDPPATH=/etc/log-to-udp.conf # - где читать конфиг
    export LOGTOUDPPATH

antibot
=======

Читает поток с udp сокета, проверяет в соответствии с настройками и логикой,
в результате пишет в memcache 

key: ip 
value: rules

Для забанненных ip, логирует запросы в mongodb.

Имеет веб интерфейс для настройки и просмотра результата работы.

Для предварительной настройки использует переменные окружения:

    ANTIBOTUSER=antibot # пользователь под которым антибот работает
    export ANTIBOTUSER
    
    ANTIBOTGROUP=antibot # группа под которой антибот работает
    export ANTIBOTGROUP
    
    ANTIBOTWWWPORT=8000 # на каком порту слушает веб интерфейс
    export ANTIBOTWWWPORT
    
    ANTIBOTUDPPORT=6001 # на каком порту принимаются логи по udp
    export ANTIBOTUDPPORT
    
    ANTIBOTSTATIC=/var/www/antibot # путь к статике для веб интерфейса
    export ANTIBOTSTATIC
    
    ANTIBOTCONFPATH=/opt/antibot # путь куда сохраняются настройки при остановке, рестарте
    export ANTIBOTCONFPATH

сборка
======

Для сборки нужны
GHC-7.4.2
haskell-platform-2012.4
+ зависимости.

    cabal configure
    cabal build

Для дебиана
после можно запустить ./build.sh для сборки deb пакетов.

Для debian squeeze есть собранные пакеты:

    log-to-udp_1.0_amd64.deb
    antibot_1.0_amd64.deb





