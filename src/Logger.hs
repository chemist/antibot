{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE RecordWildCards #-}
module Main where

import           Antibot.Data (Request)
import           Parser

import           Control.Applicative
import           Control.Concurrent           (threadDelay, forkIO)
import           Control.Monad
import           Data.Attoparsec              (Parser)
import           Data.ByteString              (ByteString, hGetSome, hGetLine, null)
import           Data.Serialize               (Serialize, encode)
import           Network.Socket               (Family (AF_INET), SockAddr,
                                               Socket, SocketType (Datagram),
                                               addrAddress, defaultProtocol,
                                               getAddrInfo, sClose, socket)
import           Network.Socket.ByteString    (sendAllTo)
import           System.IO                    (IOMode (ReadMode), hIsEOF,
                                               SeekMode (AbsoluteSeek), Handle,
                                               hFileSize, hSeek, hSetEncoding,
                                               latin1, withBinaryFile)
import           System.IO.Streams hiding (map, mapM_)
import           System.IO.Streams.Attoparsec
import           System.IO.Streams.Network
import Data.IORef
import System.Posix ( fileID, getFileStatus, FileStatus, fileExist, fileAccess)
import Control.Exception
import Data.Typeable 
import Data.ConfigFile
import Data.Either.Utils
import Prelude hiding (null)
import System.Posix.Syslog
import System.Environment
import Data.Maybe
import System.Posix.Process
import System.Posix.Signals
import System.Posix.Types
import System.Posix.IO
import System.Posix.User
import System.Exit (ExitCode(..))
import System.Directory
import System

data Config = Config 
  { file     :: !String
  , hostname :: !String
  , port     :: !String
  } deriving (Show, Eq)
        
data LoggerException = ExceptionInode deriving (Show, Typeable)

instance Exception LoggerException

main = loadLogger >>= startLogger
    
startLogger :: InitArgs -> IO ()
startLogger Log{..} = do
    when (isNothing hostname) $ usage >> errorExit
    when (isNothing port)     $ usage >> errorExit
    when (isNothing file)     $ usage >> errorExit
    checkFile (fromJust file)
    checkHost hostname port
    logger (fromJust hostname) (fromJust port) (fromJust file)
    
startLogger Server{..} | start = do
    checkUser user
    checkGroup group
    checkPid pid
    checkConf conf
    checkFile (fromJust conf)
    config <- readConfig (fromJust conf)
    prog <- getProgName
    toLog $ prog ++ " start"
    let pidFile = fromJust pid ++ "/" ++ prog ++ ".pid"
    f <- fileExist pidFile
    when f $ (print $ prog ++ " is active now, try --stop") >> errorExit
    toLog $ show config
    userId <- userID <$> getUserEntryForName (fromJust user)
    groupId <- groupID <$> getGroupEntryForName (fromJust group)
    setGroupID groupId
    setUserID userId 
    daemonize True pidFile $ do
        mapM_ (\Config{..} -> forkIO $ logger hostname port file) config 
        forever $ threadDelay 100000
        
                       | stop = do
    checkPid pid
    prog <- getProgName
    let pidFile = fromJust pid ++ "/" ++ prog ++ ".pid"
    f <- fileExist pidFile
    unless f $ (print $ prog ++ " is not active now, try --start") >> errorExit
    p <- Prelude.read <$> readFile pidFile :: IO Int
    toLog $ "stopped " ++ prog 
    signalProcess sigKILL $ (toEnum p :: CPid)
    removeFile pidFile
    where
    
    checkPid Nothing = errorExit
    checkPid (Just pidDir) = do
        f <- fileExist pidDir
        unless f $ print "pid directory does not exist" >> errorExit
        f <- fileAccess pidDir True True True
        unless f $ print "cant write pid" >> errorExit
        return ()
        
    checkConf Nothing = errorExit
    checkConf _ = return ()
    
    checkGroup Nothing = errorExit
    checkGroup (Just group) = either (const $ print "check group" >> errorExit) (const $ return ()) =<<
        (try (getGroupEntryForName group) :: IO (Either IOException GroupEntry))

    checkUser Nothing = errorExit
    checkUser (Just user) = either (const $ print "check user" >> errorExit) (const $ return ()) =<< 
        (try (getUserEntryForName user) :: IO (Either IOException UserEntry))
    
usage :: IO ()
usage = putStrLn "Help:\n\t-h -? --help\tDisplay help message"

errorExit :: IO ()
errorExit = (exitImmediately $ ExitFailure 1)

checkFile :: FilePath -> IO ()
checkFile file = do
    f <- fileExist  file
    unless f $ print "file does not exist" >> errorExit
    f <- fileAccess file True False False
    unless f $ print "cant read file, check permissions" >> errorExit
    
checkHost :: Maybe String -> Maybe String -> IO ()
checkHost hostname port = do
    addr <- try  (addrAddress . head <$> getAddrInfo Nothing hostname port) :: IO (Either IOException SockAddr)
    either (const (print "check hostname and port" >> errorExit)) (\_ -> return ()) addr
    
    
    
toLog :: String -> IO ()
toLog x = syslog Notice x
    
logger :: String -> String -> FilePath -> IO ()
logger hostname port file = forever $ do
    prog <- getProgName
    toLog $ prog ++ " start thread for " ++ file 
    addr <- addrAddress . head <$> getAddrInfo Nothing (Just hostname) (Just port)
    sock <- socket AF_INET Datagram defaultProtocol
    output <- socketToUdpStream sock addr
    try (fileToUdp file (connectTo output) streamParser) :: IO (Either SomeException ())
    threadDelay 1000000
{-# INLINE logger #-}    

socketToUdpStream :: Serialize a => Socket -> SockAddr -> IO (OutputStream a)
socketToUdpStream sock addr = makeOutputStream output
    where
    output Nothing = return $! ()
    output (Just s) = sendAllTo sock (encode s) addr 
{-# INLINE socketToUdpStream #-}


fileToUdp :: FilePath -> (InputStream Request -> IO ()) -> Parser (Maybe Request) -> IO ()
fileToUdp file out parser = do
    inode <- fileID <$> getFileStatus file
    inodeIO <- newIORef inode
    withBinaryFile file ReadMode (go inodeIO)
    where
    go inodeIO handle = do
        hSetEncoding handle latin1
        size <- hFileSize handle
        sizeRef <- newIORef size
        when (size > 1000000) $ hSeek handle AbsoluteSeek (size - 1000000) >> hGetLine handle >> return ()
        handleToInputStreamAlways handle sizeRef (doWhenEndOfFile inodeIO) >>=
          parserToInputStream parser >>= out
    doWhenEndOfFile inodeIO = do
        inode <- fileID <$> getFileStatus file
        changeInode <- atomicModifyIORef inodeIO $ \x -> (x, x /= inode)
        when changeInode $ throwIO ExceptionInode
        threadDelay 100000
{-# INLINE fileToUdp #-}

handleToInputStreamAlways :: Handle -> IORef Integer -> IO () -> IO (InputStream ByteString)
handleToInputStreamAlways h sizeRef pause = makeInputStream f
  where
  f = do
      size <- hFileSize h
      isTruncated <- atomicModifyIORef sizeRef $ \x -> (size, x > size)
      when isTruncated $ do
          toLog "file truncated"
          hSeek h AbsoluteSeek 0
      isEnd <- hIsEOF h
      if isEnd
         then pause >> f
         else do
             x <- hGetSome h bUFSIZ
             return $! if null x then Nothing else Just x
{-# INLINE handleToInputStreamAlways #-}
      
bUFSIZ :: Int
bUFSIZ = 32752

----------------------------------------------------------------------------------------------------------
-- parse config
----------------------------------------------------------------------------------------------------------
readConfig::String -> IO [Config]
readConfig path =
         do val <- readfile emptyCP path
            let cp = forceEither val
            return $ map (\x -> config x cp) $ sections cp
              
config ::  SectionSpec -> ConfigParser -> Config
config x cp = Config (forceEither $ get cp x "file")
                     x
                     (forceEither $ get cp x "port")
