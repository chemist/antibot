angular.module('antibot', []).
config(['$routeProvider', function($routeProvider) {
    $routeProvider.
    when('/banned', {
        templateUrl: 'app/banned.html',
        controller: Home
    }).
    when('/banned/:ip', {
        templateUrl: 'app/info.html',
        controller: Info
    }).
    when('/rules', {
        templateUrl: 'app/rules.html',
        controller: Rules
    }).
    when('/banrules', {
        templateUrl: 'app/banrules.html',
        controller: BanRules
    }).
    when('/whitelist', {
        templateUrl: 'app/whitelist.html',
        controller: WhiteList
    }).
    when('/status', {
        templateUrl: 'app/status.html',
        controller: Status
    }).
    otherwise({
        redirectTo: '/banned'
    });
}])


function Home($scope, $http, $routeParams) {
    $scope.page = "banned";
    $scope.paginator = {
        sort: "ip",
        asc: true,
        page: 0
    };
    $scope.size = [];
    $http.get('size').success(function(data) {
        var i = parseInt(data);
        for (x=0; x <= i; x++) {
           $scope.size.push(x);
        };
    });
    $scope.showDate = function(x) {
        var result = new Date(x);
        return result.toString();
    };
    $('.menu.active').removeClass('active');
    $('#banned').addClass('active');
    $scope.banned = [];
    $scope.toPage = function(x, index) {
        $scope.paginator = { sort: $scope.paginator.sort,
                             asc:  $scope.paginator.asc,
                             page: x
        };
        $http.post('banned', $scope.paginator).success(function(data) {
            $scope.banned = data;
        });
        $('.pagination ul li').removeClass('active');
        $('.pagination ul li#' + index).addClass('active');
    };
    $scope.toPage(0,0);
        
    $scope.sortBy = function(t, reverse) {
        var span = "table.tablesorter thead tr a#" + t + " span";
        $('table.tablesorter thead tr a span').removeClass('caret');
        $(span).addClass('caret');
        if (reverse === true) {
           $('table.tablesorter thead tr a').addClass('dropup');
        } else {
           $('table.tablesorter thead tr a').removeClass('dropup');
        };
        if (reverse === undefined) { var r = false } else { var r = reverse };
        $scope.paginator = { sort: t, asc:r, page: $scope.paginator.page};
        $http.post('banned', $scope.paginator).success(function(data) {
            $scope.banned = data;
        });
    };
};

function Info($scope, $http, $routeParams) {
    var path = 'banned/' + $routeParams.ip; 
    $scope.page = "info";
    $scope.showDate = function(x) {
        var result = new Date(x);
        return result.toString();
    };
    $http.get(path).success(function(data) {
        $scope.info = data;
    });
};
function BanRules($scope, $http, $routeParams) {
    $scope.page = "banrules";
    $http.get('banrules').success(function(data) {
        $scope.banrules = data;
        $scope.addban.rules = [];
    });
    $http.get('rules').success(function(data) {
        $scope.rules = data;
    });
    $scope.addRuleToForm = function(x) {
        $scope.addban.rules.push(x);
        $scope.addban.cp = null;
    };
    $scope.removeBanCart = function(x) {
        $scope.addban.rules = $scope.addban.rules.filter(function(y) {
            return (x != y)
        });
    };

    $scope.addBanRule = function(x) {
        var ban = {
            banName: x.name,
            rules: $scope.addban.rules
        };
        $http.post('banrules', ban).success(function(data) {
            console.log(data);
            $scope.banrules = data;
            $scope.addban = {};
            $scope.addban.rules = [];
        });
    };
    $scope.removeBanRule = function(x) {
        $http.post('banrules/rm', x).success(function(data) {
            $scope.banrules = data;
            console.log(x);
        });
    };
    $('.menu.active').removeClass('active');
    $('#banrules').addClass('active');
};

function Rules($scope, $http, $routeParams) {
    $http.get('rules').success(function(data) {
        $scope.rules = data;
    });
    $scope.removeRule = function(x) {
        $http.post('rules/rm', x).success(function(data) {
            $scope.rules = data;
            console.log(x);
        });
    };
    $scope.jstype = function(x) {
        return x.type === "js"
    };
    $scope.periodtype = function(x) {
        return x.type === "period"
    };
    $scope.regexptype = function(x) {
        return x.type === "regexp"
    };
    $scope.page = "rules";
    $('.menu.active').removeClass('active');
    $('#rules').addClass('active');

    $scope.selectTypeRule = function() {
        $('.addRule').addClass('hidden');
        if ($scope.ruleType === "js") {
            $('#jsform').removeClass('hidden');
            $scope.addjs.path = [];
            $scope.addjs.countpath = [];
        };
        if ($scope.ruleType === "regexp") {
            $('#regexpform').removeClass('hidden');
            $scope.addregexp.urlquery = [];
        };
        if ($scope.ruleType === "period") {
            $('#periodform').removeClass('hidden');
            $scope.addperiod.countpath = [];
        };
    };
    $scope.addJsRule = function(x) {
        var rule = {
            name: x.name,
            path: x.path,
            countPath: x.countpath,
            maxReqWithoutJs: parseInt(x.maxReq),
            type: "js"
        };
        $http.post('rules', rule).success(function(data) {
            $scope.rules = data;
            $scope.addjs = {};
            $scope.addjs.countpath = [];
            $scope.addjs.path = [];
        });
    };
    $scope.addPath = function(x) {
        if (x) {
            $scope.addjs.path.push(x);
            $scope.addjs.p = null;
        };
    };
    $scope.addCountPath = function(x) {
        if (x) {
            $scope.addjs.countpath.push(x);
            $scope.addjs.cp = null;
        };
    };
    $scope.addPeriodCountPath = function(x) {
        if (x) {
            $scope.addperiod.countpath.push(x);
            $scope.addperiod.cp = null;
        };
    };
    $scope.addUrlQuery = function(url, query) {
        if (url && query) {
            $scope.addregexp.urlquery.push({
                url: url,
                query: query
            });
            $scope.addregexp.path = null;
            $scope.addregexp.query = null;
        };
    };
    $scope.removeRegexpCart = function(url, query) {
        $scope.addregexp.urlquery = $scope.addregexp.urlquery.filter(function(x) {
            return (x.url != url && x.query != query)
        });
    };
    $scope.removePeriodCart = function(x) {
        $scope.addperiod.countpath = $scope.addperiod.countpath.filter(function(y) {
            return (x != y)
        });
    };
    $scope.removeJsCart = function(x) {
        $scope.addjs.path = $scope.addjs.path.filter(function(y) {
            return (x != y)
        });
    };
    $scope.removeJsCart1 = function(x) {
        $scope.addjs.countpath = $scope.addjs.countpath.filter(function(y) {
            return (x != y)
        });
    };
    $scope.addPeriodRule = function(x) {
        var rule = {
            name: x.name,
            countPath: x.countpath,
            period: parseInt(x.period),
            countByPeriod: parseInt(x.countperiod),
            type: "period"
        };
        $http.post('rules', rule).success(function(data) {
            $scope.rules = data;
            $scope.addperiod = {};
            $scope.addperiod.countpath = [];
        });
    };
    $scope.addRegexpRule = function(x) {
        var rule = {
            name: x.name,
            urlQuery: x.urlquery,
            regexp: x.regexp,
            type: "regexp"
        };
        $http.post('rules', rule).success(function(data) {
            $scope.rules = data;
            $scope.addregexp = {};
            $scope.addregexp.urlquery = [];
        })
    };
};


function WhiteList($scope, $http, $routeParams) {
    $scope.page = "whitelist";
    $('.menu.active').removeClass('active');
    $('#whitelist').addClass('active');
    $scope.white = [];
    $http.get('white').success(function(data) {
        $scope.white = data;
    });
    $scope.addSubnet = function(x) {
        var sub = {
            type: "subnet",
            subnet: x.subnet
        };
        $http.post('white', sub).success(function(data) {
            $scope.white = data;
            $scope.addwhite = {};
        });
    };
    $scope.removeSubnet = function(x) {
        $http.post('white/rm', x).success(function(data) {
            $scope.white = data;
        });
    };
};

function Status($scope, $http, $routeParams) {
    $scope.page = "status";
    $('.menu.active').removeClass('active');
    $('#status').addClass('active');
    $scope.process = [];
    $http.get('process').success(function(data) {
        $scope.process = data;
    });
    $http.get('membase').success(function(data) {
        $scope.membase = data;
    });
    $http.get('mongo').success(function(data) {
        $scope.mongo = data;
    });
    $scope.addMembase = function(x) {
        var con = {
            hostname: x.hostname,
            port: x.port,
            type: "membase"
        };
        $http.post("membase", con).success(function(data) {
            $scope.membase = data;
            $scope.memb = {};
        });
    };
    $scope.addMongo = function(x) {
        var con = {
            hostname: x.hostname,
            port: x.port,
            type: "mongo"
        };
        $http.post("mongo", con).success(function(data) {
            $scope.mongo = data;
            $scope.mong = {};
        });
    };
    $scope.clearBase = function() {
        $http.get('clearbase');
    };
};
    


