{-# LANGUAGE OverloadedStrings #-}
module Parser where

import Antibot.Data (Request(..))
import Data.Attoparsec hiding (inClass, takeTill, takeWhile)
import Data.Attoparsec.Char8 
import Control.Applicative
import Data.Network.Ip
import Data.Bits
import Data.Time hiding (parseTime)
import Prelude hiding (take, takeWhile)
import Data.Fixed (Pico, resolution)
import Data.ByteString (ByteString)
import Network.HTTP.Types hiding (parseMethod)
import Debug.Trace

-- 217.66.152.255 - - [20/Jun/2013:17:20:25 +0400] 1.204 "GET /index.php?s=%D0%B7%D0%B0%D0%B9%D1%86%D0%B5%D0%B2+%D0%BD%D0%B5%D1%82&&fs=plugin.ff HTTP/1.1" 200 23582 "-" "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36" "www.nigma.ru"

streamParser :: Parser (Maybe Request)
streamParser = (endOfInput >> pure Nothing) <|> parseRequest

parseRequest :: Parser (Maybe Request)
parseRequest = do
    remoteAddr <- parseIp <* space
    remoteUser <- takeTill isSpace <* (space >> takeWhile (/= '[')) 
    localTime <- parseTime <* space
    requestTime <- takeTill isSpace <* (space >> db)
    method <- parseMethod <* space
    request <- parseR <* (db >> space)
    status <- parseStatus <* space
    bodyBytesSent <- takeTill isSpace <* (space >> db)
    httpReferer <- takeWhile (/= '"') <* (anyChar >> space >> db)
    httpUserAgent <- takeWhile (/= '"') <* (anyChar >> space >> db)
    host <- takeWhile (/= '"') <* anyChar
    endOfLine
    return $ Just Request
      { rIp = remoteAddr
      , rTime = localTime
      , reqTime = requestTime
      , rRequest = request
      , rType = method
      , rStatusCode = status
      , rBytesSend = bodyBytesSent
      , rHttpRefer = httpReferer
      , rUserAgent = httpUserAgent
      , rHost = host
      }
                                                                                
db = char8 '"'
    
parseIp::Parser IP
parseIp = do
    a <- fromIntegral <$> octet
    b <- fromIntegral <$> octet
    c <- fromIntegral <$> octet
    d <- fromIntegral <$> decimal
    return $ IPv4 (a `shift` 24 + b `shift` 16 + c `shift` 8 + d)
    
octet::Parser Int
octet = decimal <* char '.'

-- [30/Jun/2012:00:00:01 +0400]
parseTime :: Parser UTCTime
parseTime = char8 '[' *> pTime <* char8 ']'

pTime::Parser UTCTime
pTime = do
    day <- decimal <* char '/'
    month <- monthToInt <$> take 3
    char '/' 
    year <- decimal <* char ':'
    hour <- decimal <* char ':'
    minuts <- decimal <* char ':'
    sec <- decimal <* space
    anyChar
    add <- decimal
    let zone = minutesToTimeZone (60 * addH add + addM add)
        localtime = LocalTime (fromGregorian year month day) (TimeOfDay hour minuts $ topico sec)
    return $! localTimeToUTC zone localtime
    where addH = flip div 100
          addM = flip rem 100
          

topico::Int -> Pico
topico val = toEnum val * (fromInteger $ resolution (toEnum val :: Pico))


monthToInt::ByteString -> Int
monthToInt "Jan" = 1         
monthToInt "Feb" = 2
monthToInt "Mar" = 3
monthToInt "Apr" = 4
monthToInt "May" = 5
monthToInt "Jun" = 6
monthToInt "Jul" = 7
monthToInt "Aug" = 8
monthToInt "Sep" = 9
monthToInt "Oct" = 10
monthToInt "Nov" = 11
monthToInt "Dec" = 12
monthToInt _ = undefined

parseMethod :: Parser StdMethod
parseMethod =  (string "GET" >> pure GET)
           <|> (string "POST" >> pure POST)
           <|> (string "PUT" >> pure PUT)
           <|> (string "HEAD" >> pure HEAD)
           <|> (string "DELETE" >> pure DELETE)
           <|> (string "TRACE" >> pure TRACE)
           <|> (string "CONNECT" >> pure CONNECT)
           <|> (string "OPTIONS" >> pure OPTIONS)
    

parseStatus :: Parser Status
parseStatus = decimal >>= pure . toEnum

-- /index.php?s=%D0%B7%D0%B0%D0%B9%D1%86%D0%B5%D0%B2+%D0%BD%D0%B5%D1%82&&fs=plugin.ff HTTP/1.1
--
parseR :: Parser (ByteString, Query)
parseR = do
    uri <- takeTill endUri
    q <- parseQuery <$> takeTill isSpace
    takeWhile (/= '"')
    return (uri, q)
    

endUri = inClass "?# "
