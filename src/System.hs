{-# LANGUAGE DeriveDataTypeable #-}
module System where

import System.Posix.Process
import System.Posix.IO
import System.Exit (ExitCode(..))
import Control.Monad

import System.Console.CmdArgs.Implicit
import System.Environment (getProgName)

daemonize :: Bool -> String -> IO () -> IO ()
daemonize exit pidFile program = do
    _ <- forkProcess child1
    when exit $ exitImmediately ExitSuccess
    where 
    child1 = do
        _ <- createSession
        pid <- forkProcess child2
        writeFile pidFile $ show pid
        exitImmediately ExitSuccess
        
    child2 = do
        mapM_ closeFd [stdInput, stdOutput, stdError]
        nullFd <- openFd "/dev/null" ReadWrite Nothing defaultFileFlags
        mapM_ (dupTo nullFd) [stdInput, stdOutput, stdError]
        closeFd nullFd
        program

version :: String
version = "2.0"

data InitArgs = Server
  { user        :: Maybe String
  , group       :: Maybe String
  , pid         :: Maybe String
  , conf        :: Maybe String
  , start:: Bool
  , stop:: Bool
  }           | Log
  { hostname :: Maybe String
  , file     :: Maybe String
  , port     :: Maybe String
  }  deriving (Show, Data, Typeable)
  

dummyServer :: InitArgs
dummyServer = Server
  { user = Nothing
  , group = Nothing
  , pid = Nothing
  , conf = Nothing
  , start = False
  , stop = False
  }
  
dummyOne :: InitArgs
dummyOne = Log
  { hostname = Nothing
  , file     = Nothing
  , port     = Nothing
  }
  
server :: Annotate Ann
server = record dummyServer
  [ user  := def += typ "USER"  += help "set user for daemon"
  , group := def += typ "GROUP" += help "set group for daemon"
  , pid   := def += typDir      += help "set path to pid file"
  , conf  := def += typFile     += help "set path to config file"
  , start := def += help "use --start for start server"
  , stop  := def += help "use --stop for stop server"
  ] += help "start, stop logger server" 

one :: Annotate Ann
one = record dummyOne
  [ hostname := def += typ "HOSTNAME" += help "host with antibot server"
  , file     := def += typFile        += help "log file"
  , port     := def += typ "PORT"     += help "udp port on antibot server"
  ] += help "start logger for one log file"
  
full :: String -> Annotate Ann
full progName = modes_ [ server, one += auto]
    += helpArg [name "h", groupname "Help"]
    += versionArg [groupname "Help"]
    += program progName
    += summary (progName ++ ": " ++ version)
  
loadLogger :: IO InitArgs
loadLogger = do
    progName <- getProgName
    cmdArgs_ (full progName)
