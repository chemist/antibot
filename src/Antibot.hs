{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE BangPatterns #-}
module Main where

import Control.Concurrent (forkIO, threadDelay)
import Control.Concurrent.STM (TMVar, readTMVar, putTMVar, takeTMVar, newTMVarIO, newEmptyTMVarIO, newTChanIO, atomically, newBroadcastTChanIO, dupTChan, TChan, readTChan, newTMVar)
import Control.Exception (try, finally, SomeException)
import Control.Applicative ((<$>))
import Control.Monad.Reader (liftIO, forever, ask, runReaderT, void, when)
import Control.Concurrent.MVar (MVar, newMVar, newEmptyMVar, withMVar, modifyMVar_, modifyMVar)
import Control.Monad.State (execStateT)

import Data.Aeson (decode, encode)
import Data.Time (getCurrentTime, diffUTCTime)
import Data.Maybe (fromMaybe)
import Data.Network.Ip (IPSubnet)
import Data.Map (empty, member, update, filter, size, intersection, toList, Map )
import qualified Data.Set as Set
import Data.Set (fromList, Set)
import Data.ByteString.Char8 (pack, ByteString)
import qualified Data.ByteString.Lazy as BL

import Text.Regex.PCRE.Light (compile)

import System.IO  (hSetBuffering, stdout, BufferMode( LineBuffering ) )
import System.Environment (getEnvironment)
import System.Posix.Daemonize (serviced, CreateDaemon(..))
import System.Exit (exitSuccess)
import System.Posix.Syslog (syslog, Priority(Notice))
import System.Posix.Signals (installHandler, Handler(Catch), sigTERM, fullSignalSet)

import Happstack.Server (bindPort, ServerPart, mapServerPartT, logMAccess, logAccess, port, simpleHTTPWithSocket, nullConf)
import Network.Socket ( Socket
                      , getAddrInfo
                      , defaultHints
                      , socket
                      , addrFamily
                      , defaultProtocol
                      , setSocketOption
                      , SocketOption( ReuseAddr ) 
                      , bindSocket
                      , addrAddress
                      , withSocketsDo
                      , sClose
                      , SocketType( Datagram )
                      , AddrInfoFlag( AI_PASSIVE )
                      , addrFlags
                      )
                      
import Antibot.Web (web)
import Antibot.Handler.Period (perioder, Counter(..))
import Antibot.Handler.Regex (regexper, Regexper(..))
import Antibot.Handler.Js (jser, Jser(..))
import Antibot.Handler.BotNet 
import Antibot.Logger (logger, Logger(..), closeMongo)
import Antibot.Ban (Ban(..), banE) 
import Antibot.Store (storeT, Store(..), closeMembaseConnect)
import qualified Antibot.Input as IN
import Antibot.Data ( Rule(..)
                    , Rules
                    , BanRule(..)
                    , BanRules
                    , SM
                    , TM
                    , Process(..)
                    , Result(..)
                    , get
                    , UrlQuery(..)
                    , ServerSideT
                    , register
                    , banMessage
                    , Connect
                    , MembaseConnect
                    , MongoConnect
                    , Antibot(..)
                    , Application(..)
                    , runAntibot
                    , rm
                    , Name
                    , Request(..)
                    )

import Prelude hiding (putStrLn, catch, filter)


main::IO ()
main = do
    args <-  getEnvironment
    let httpPort = fromMaybe 8000 $ (read <$> lookup "ANTIBOTWWWPORT" args::Maybe Int)
    let udpPort = fromMaybe "5001" $ lookup "ANTIBOTUDPPORT" args
        user' = fromMaybe "root" $ lookup "ANTIBOTUSER" args
        group' = fromMaybe "root" $ lookup "ANTIBOTGROUP" args
        staticPath = fromMaybe "/var/www/antibot" $ lookup "ANTIBOTSTATIC" args
        confPath = fromMaybe "/tmp" $ lookup "ANTIBOTCONFPATH" args
    serviced $ daemon confPath user' group' staticPath httpPort udpPort

runFromRoot::Int -> String -> IO (Socket,Socket)
runFromRoot httpP udpP= do
    http <- bindPort $ nullConf { port = httpP }
    udp <- openSock udpP
    return (http, udp)
    
daemon::String -> String -> String -> String -> Int -> String -> CreateDaemon (Socket, Socket)
daemon confPath user' group' staticPath httpP udpP = 
     CreateDaemon { privilegedAction = runFromRoot httpP udpP
                  , program = main' confPath staticPath
                  , name = Just "antibot"
                  , user = Just user'
                  , group = Just group'
                  , syslogOptions = []
                  , pidfileDirectory = Just "/var/run/antibot"
                  }
    
antibot :: IO ()
antibot = do
    let httpPort = 8000
        udpPort = "5001"
        user = "chemist"
        group = "chemist"
        confPath = "/tmp"
        staticPath = "src/static"
    sockets <- runFromRoot httpPort udpPort
    main' confPath staticPath sockets
    
main' :: String -> String -> (Socket,Socket) -> IO ()
main' confPath staticPath (web,udp) = do
    hSetBuffering stdout LineBuffering
    !server <- initServ udp web staticPath confPath
    installHandler sigTERM (Catch $ haveHup server confPath) (Just fullSignalSet)
    runAntibot server serv 
            
haveHup server confPath = do
    syslog Notice "Have sig TERM "
    syslog Notice "Write config "
    a <- try $ runAntibot server (writeConf confPath)  ::IO  (Either SomeException ())
    case a of
         Left x -> syslog Notice $ "Have error " ++ show x
         Right x -> syslog Notice $ "Exit success"
    exitSuccess
    
runApplication::Application a -> Antibot -> ServerPart a
runApplication (Application x) antibot = mapServerPartT (runAntibot antibot) x

openSock::String -> IO Socket
openSock port = do
       addrinfos <- getAddrInfo 
                    (Just (defaultHints {addrFlags = [AI_PASSIVE]}))
                    Nothing (Just port)
       let serveraddr = head addrinfos
       sock <- socket (addrFamily serveraddr) Datagram defaultProtocol
       setSocketOption sock ReuseAddr 1
       bindSocket sock (addrAddress serveraddr)
       return sock
  
initServ::Socket -> Socket -> String -> String -> IO Antibot
initServ udp web staticPath confPath = do
    !rules <-  fromMaybe [] . decode <$> tryReadFile (confPath ++ "/rules.conf")::IO Rules
    tmrules <- newTMVarIO rules
    !brules <- fromMaybe [] . decode <$> tryReadFile (confPath ++ "/banrules.conf")::IO BanRules
    tmbrules <- newTMVarIO brules
    !whiteM <- fromMaybe [] . decode <$> tryReadFile (confPath ++ "/white.conf")::IO [IPSubnet]
    white <- newTMVarIO whiteM
    !membaseM <- decode <$> tryReadFile (confPath ++ "/membase.conf")::IO (Maybe (Connect MembaseConnect))
    membase <- case membaseM of
                  Nothing -> newEmptyTMVarIO
                  Just x -> newTMVarIO x
    membaseConnect <- newEmptyTMVarIO
    !mongoM <- decode <$> tryReadFile (confPath ++ "/mongo.conf")::IO (Maybe (Connect MongoConnect))
    mongo' <- case mongoM of
                    Nothing -> newEmptyTMVarIO
                    Just x -> newTMVarIO x
    mongoPipe' <- newEmptyTMVarIO
    requests <- newBroadcastTChanIO
    result <- newBroadcastTChanIO
    banMes <- newTChanIO
    banIp <- newTMVarIO empty
    ts <- newTMVarIO empty
    status <- newTMVarIO []
    return $! Antibot { reqT = requests
                      , result = result
                      , banMessage = banMes
                      , timeState = ts
                      , sock = udp
                      , websock = web
                      , static = staticPath
                      , state = status
                      , startP = startProcess
                      , startH = makeSomeHandlers
                      , banrules = tmbrules
                      , rulesA = tmrules
                      , banIp = banIp
                      , whiteList = white
                      , membase = membase
                      , membaseConnect = membaseConnect
                      , mongoPipe = mongoPipe'
                      , mongo = mongo'
                      }
                     
writeConf::String -> ServerSideT ()
writeConf confPath = do
    rules <- get::ServerSideT Rules
    banrules <- get::ServerSideT BanRules 
    white <- get::ServerSideT [IPSubnet]
    membase <- get::ServerSideT (Maybe (Connect MembaseConnect))
    mongo <- get::ServerSideT (Maybe (Connect MongoConnect))
    processes <- get::ServerSideT [Process]
    liftIO $ do BL.writeFile (confPath ++ "/rules.conf") $ encode rules
                syslog Notice  "write rules"
                BL.writeFile (confPath ++ "/banrules.conf") $ encode banrules
                syslog Notice  "write banrules"
                BL.writeFile (confPath ++ "/membase.conf") $ encode membase
                syslog Notice  "write membase"
                BL.writeFile (confPath ++ "/mongo.conf") $ encode mongo
                syslog Notice  "write mongo"
                BL.writeFile (confPath ++ "/white.conf") $ encode white
                syslog Notice  "write white"
    liftIO $ syslog Notice "Stopping all presesses"
    liftIO $ syslog Notice  $ show processes
    !_ <- mapM_ rm processes
    liftIO $ syslog Notice  "Finish stopping processes"
    return ()
    
    
                     
tryReadFile::String -> IO BL.ByteString
tryReadFile path = do
    !x <- liftIO $ try (BL.readFile path)::IO  (Either SomeException BL.ByteString)
    case x of
         Left _ -> return BL.empty
         Right a -> return a
    
                     
serv::ServerSideT ()
serv = mapM_ startProcess ["banHandler", "counterHandler", "makeHandlers", "getRequest", "store", "cleanerS", "logger", "web" ]

startProcess::Name -> ServerSideT ()
startProcess "banHandler" = banHandler
startProcess "showResult" = showResult
startProcess "makeHandlers" = makeHandlers
startProcess "getRequest" = getRequest
startProcess "counterHandler" = counterHandler
startProcess "cleanerS" = cleanerS
startProcess "store" = store
startProcess "logger" = loggerS
startProcess "web" = do
    server <- ask
    liftIO $ simpleHTTPWithSocket (websock server) (nullConf { logAccess = Just $ logMAccess }) $ runApplication web server
startProcess _ = fail "unknown process name"
     
-- | get request from udp, parse, write to chan
getRequest::ServerSideT ()
getRequest = do
    liftIO $ syslog Notice  "start getRequest"
    sock <- get
    requests <- reqT <$> ask -- | write to broadcast tchan
    pid <- liftIO $ forkIO $ withSocketsDo $ finally (IN.input sock requests)
                                                    (sClose sock)
    register $ Process ("getRequest", Just pid)
     
-- | get result and if ip was ban, log to mongo
loggerS::ServerSideT ()
loggerS = do
    liftIO $ syslog Notice  "start logger"
    input <- get >>= liftIO . atomically . dupTChan
    mongoC <- ask
    mongoP <- ask
    banIP <- get
    let loggerST = Logger (input::TChan Request) banIP (mongo mongoC) (mongoPipe mongoP)
    pid <- liftIO $ forkIO $ finally (void $ runReaderT logger loggerST)
                                    (void $ do
                                        syslog Notice "logger was closed, you must restart antibot"
                                        runReaderT closeMongo loggerST)
    register $ Process ("logger", Just pid)
     
-- | start handlers by rules
makeHandlers::ServerSideT ()
makeHandlers = do
    liftIO $ syslog Notice  "start makeHandlers"
    rulesA <- get
    p <- mapM handler rulesA
    mapM_ register p
    
-- | start handlers by name
makeSomeHandlers::Rules -> ServerSideT ()
makeSomeHandlers r = do
    p <- mapM handler r
    mapM_ register p
    
-- | handler by period
handler::Rule -> ServerSideT Process
handler rule@(Period{..}) = do
    input <- get >>= liftIO . atomically . dupTChan
    output <- get
    pid <- liftIO $ forkIO $ do
        timer <- atomically $ newTMVar True
        void . execStateT perioder $ Counter input output timer rule Set.empty empty $ makePathSet rule countPath'
    return $ Process ("handler." ++ name, Just pid)
-- | handler by regexp
handler rule@(Regexp{..}) = do
    input <- get >>= liftIO . atomically . dupTChan
    output <- get
    pid <- liftIO $ forkIO $ do
       void . execStateT regexper $ Regexper input output rule (compile (pack regexp) []) $ makeUrlSet rule
    return $ Process ("handler." ++ name, Just pid)
-- | handler by js
handler rule@(Js{..}) = do
    liftIO $ syslog Notice  "start js"
    input <- get >>= liftIO . atomically . dupTChan
    output <- get
    pid <- liftIO $ forkIO $ do
       var <- atomically $ newTMVar empty
       void . execStateT jser $ Jser input output rule var (makePathSet rule path') $ makePathSet rule countPath'
    return $ Process ("handler." ++ name, Just pid)
    {-
handler rule@(Botnet{..}) = do
    input <- get >>= liftIO . atomically . dupTChan
    output <- get
    i <- liftIO $ newTMVarIO 0
    pid <- liftIO $ forkIO $ do
        void . runReaderT killBotNet $ Boter input output rule Set.empty i
    return $ Process ("handler." ++ name, Just pid)
    -}
    
-- | read result from handlers, check when ban, write ip map and result to chan for store
banHandler::ServerSideT ()
banHandler = do
    liftIO $ syslog Notice  "start banHandler"
    result <- get >>= liftIO . atomically . dupTChan
    toStore <- banMessage <$> ask
    banr <- banrules <$> ask
    tBanIp <- get::ServerSideT (TMVar SM)
    pid <- liftIO $ forkIO $ runReaderT banE $ Ban result toStore banr tBanIp
    register (Process ("banHandler", Just pid))

-- | write to membase when ban
store::ServerSideT ()
store = do
    liftIO $ syslog Notice  "start store"
    membaseS <- membase <$> ask
    membaseC <- membaseConnect <$> ask
    toStore <- banMessage <$> ask
    white <- whiteList <$> ask
    ts <- timeState <$> ask
    let storeST = Store membaseS membaseC toStore white ts
    pid <- liftIO $ forkIO $ finally (void $ runReaderT storeT storeST)
                                    (void $ runReaderT closeMembaseConnect storeST)
    register (Process ("store", Just pid))

-- | set counter by requests
counterHandler::ServerSideT ()
counterHandler = do
    liftIO $ syslog Notice  "start counterHandler"
    requestTChannel <- get >>= liftIO . atomically . dupTChan
    tBanIp <- get
    pid <- liftIO $ forkIO $ forever $ do
        !r <- atomically $ readTChan requestTChannel
        let !ipC = rIp r
        !isBan <- atomically $ readTMVar tBanIp >>= \x -> return $ member (rIp r) x
        when isBan $ atomically $ do
            ban'' <- takeTMVar tBanIp
            putTMVar tBanIp $ update (\x -> Just $ x { countM = succ (countM x)}) ipC ban''
        return ()
    register (Process ("counterHandler", Just pid))

-- | clean SM and TM map after 24h
cleanerS::ServerSideT ()
cleanerS = do
    liftIO $ syslog Notice  "cleaner start"
    baned <- get::ServerSideT (TMVar SM)
    timed <- get::ServerSideT (TMVar TM)
    pid <- liftIO $ forkIO $ forever $ do
        syslog Notice  "start cleaner"
        now <- getCurrentTime
        (!timed', !s) <- atomically $ do
            x <- takeTMVar timed
            let new = filter (\y -> diffUTCTime now y < 24 * 3600) x 
            putTMVar timed new
            return (new, size new)
        atomically $ do
            x <- takeTMVar baned
            putTMVar baned (intersection x timed')
        syslog Notice  "stop cleaner"
        syslog Notice  $ "start from" ++ show s
        syslog Notice  $ "stop when" ++ (show $ size timed')
        threadDelay $ 1000000 * 10 * 60
        return ()
    register (Process ("cleanerS", Just pid))
        
showC ::  Show a => Map a Result -> [Char]
showC a = Prelude.concatMap (\(x,y) -> show x ++ (show $ countM y) ++ "\n\n") $ toList a

testBan::BanRules
testBan = [BanRule "jscript" [Regexp "reg" "\\(.*\\)\\s*\\|+\\s*\\(.*\\)" [UrlQuery (pack "/index.php") (pack "s")]]]

showResult::ServerSideT ()
showResult = do
    liftIO $ syslog Notice  "start showResult"
    resultTChanel <- get >>= liftIO . atomically . dupTChan -- | for read broadcast tchan
    pid <- liftIO $ forkIO $ forever $ do   
        res <- atomically $ readTChan resultTChanel
        syslog Notice  $ (show $ ip' res) ++ show (Antibot.Data.name $ head $ ruleState res)
    register $ Process ("showResult", Just pid)
   
path', countPath'::Rule -> [String]
path' = path
countPath' = countPath

makePathSet::Rule -> (Rule -> [String]) -> Set ByteString
makePathSet rule f = fromList $ map pack $ f rule
                     
makeUrlSet::Rule -> Set UrlQuery
makeUrlSet rule = fromList $ urlQuery rule
                   
testRules::Rules
testRules = [ Period "base" 10 10 ["/index.php"]
            , Regexp "reg" "\\(.*\\)\\s*\\|+\\s*\\(.*\\)" [UrlQuery (pack "/index.php") (pack "s")]
            , Js "js" ["/user_time_log.php","/yandex_adv_log.php"] 5 ["/index.php"]
            ]
            
            
