{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
module Antibot.Data 
( Request(..)
, Rule(..)
, Result(..)
, UrlQuery(..)
, parseIp
, TM
, Connect(..)
, MongoConnect(..)
, MembaseConnect(..)
, Application(..)
, Process(..)
, Rules
, SM
, BanRule(..)
, BanRules
, put
, get
, rm
, ruleToName
, eithToMaybe
, clearBase
, ServerSideT
, register
, Antibot(..)
, runAntibot
, Name
, tryReadMVar
) 
where

import Control.Concurrent ( ThreadId, killThread )
import Data.Time (UTCTime(..), Day(ModifiedJulianDay) )
import Control.Concurrent.STM ( atomically, readTMVar, putTMVar, swapTMVar, takeTMVar, tryTakeTMVar, TMVar, TChan, tryReadTMVar )
import Control.Concurrent.MVar ( MVar, tryTakeMVar, putMVar, isEmptyMVar, readMVar, swapMVar, modifyMVar, modifyMVar_, withMVar )
import Control.Exception (mask_)
import Data.Network.Ip ( IP(..), IPSubnet(..), Mask(Mask32), network )
import Data.Bits (shift, shiftL, shiftR)
import GHC.Word (Word32)
import Data.Text.Encoding (encodeUtf8)
import Text.Regex.PCRE.Light (compileM)
import Data.ByteString (ByteString)
import Network.Socket (Socket, PortNumber)
import Prelude hiding (putStrLn)
import Data.Text (Text)
import Control.Monad.Reader (liftIO, runReaderT, mzero, void, ReaderT, MonadIO, MonadReader, MonadPlus, when)
import Happstack.Server (ServerPartT, HasRqData, ServerMonad, WebMonad(..), Response, FilterMonad(..), Happstack, toResponse)
import Control.Applicative (pure, (<$>), (<*), (<*>), (<|>), Applicative, Alternative)
import Data.Aeson (ToJSON(..), FromJSON(..), (.=), object, (.:), Value(..))
import GHC.Generics (Generic)
import Data.Maybe (isJust, fromJust)
import Data.List (take, isPrefixOf)
import Data.Attoparsec (choice)
import Data.Aeson.Types (Parser, Object)
import Database.MongoDB (close, runIOE, connect, Host(..), PortID(PortNumber), Pipe)
import Data.Map (Map, toList, empty)
import qualified Network.Memcache.Protocol as MC
import qualified Data.Attoparsec.ByteString.Char8 as P
import qualified Data.ByteString.Char8 as B8
import qualified Control.Monad.Reader as Reader
import System.Posix.Syslog (syslog, Priority(Notice))
import Data.Set (Set)
import qualified Data.Set as Set
import Network.HTTP.Types
import qualified Data.Serialize as Se

type Name = String
type StatusIp = Bool
type Rules = [Rule]
type BanRules = [BanRule]
type Banneds = [Banned]
type SM = Map IP Result
type TM = Map IP UTCTime

newtype Banned = Banned (IP, Result) deriving (Show, Eq, Ord)
newtype Process = Process (Name, Maybe ThreadId) 

newtype Application a = Application { unApp' :: ServerPartT ServerSideT a }
     deriving ( Functor, Alternative, Applicative, Monad, MonadPlus, MonadIO
              , HasRqData, ServerMonad ,WebMonad Response, FilterMonad Response
              , Happstack, MonadReader Antibot)
              
type ServerSideT = ReaderT Antibot IO

runAntibot:: Antibot -> ServerSideT a -> IO a
runAntibot s x = runReaderT x s

data Request = Request
  { rIp :: !IP
  , rTime :: !UTCTime
  , reqTime :: !ByteString
  , rRequest :: !(ByteString, Query)
  , rType :: !StdMethod
  , rStatusCode :: !Status
  , rBytesSend  :: !ByteString
  , rHttpRefer  :: !ByteString
  , rUserAgent  :: !ByteString
  , rHost       :: !ByteString
  } deriving (Show, Eq, Ord)
  
 
instance Se.Serialize UTCTime where
    put (UTCTime (ModifiedJulianDay day) diff) = Se.put day >> (Se.put $ fromEnum diff)
    get = UTCTime <$> (ModifiedJulianDay <$> Se.get) <*> (toEnum <$> Se.get)
    
instance Se.Serialize StdMethod where
    put x = Se.put $ fromEnum x
    get = toEnum <$> Se.get
   
instance Se.Serialize Status where
    put x = Se.put $ fromEnum x
    get = toEnum <$> Se.get
    
instance Se.Serialize Request where
    put Request{..} = do
        Se.put rIp
        Se.put rTime
        Se.put reqTime
        Se.put rRequest
        Se.put rType
        Se.put rStatusCode
        Se.put rBytesSend
        Se.put rHttpRefer
        Se.put rUserAgent
        Se.put rHost
    get = Request <$> Se.get 
                  <*> Se.get
                  <*> Se.get
                  <*> Se.get
                  <*> Se.get
                  <*> Se.get
                  <*> Se.get
                  <*> Se.get
                  <*> Se.get
                  <*> Se.get
  

{-
data Req = Req { ip:: !IP
               , lTime:: !UTCTime
               , reqTime:: !B8.ByteString
               , req:: !(B8.ByteString, [(B8.ByteString, B8.ByteString)])
               , reqType:: !B8.ByteString
               , statusCode:: !B8.ByteString
               , bytesSent:: !B8.ByteString
               , httpRefer:: !B8.ByteString
               , userAgent:: !B8.ByteString
               , hostR:: !B8.ByteString
               } deriving (Eq, Ord, Show)

-}

data Result = Result 
  { timeM:: !UTCTime
  , ruleState:: !Rules
  , countM:: !Int
  , ip':: !IP
  , banned:: !StatusIp 
  }         | CleanJS 
  { ip' :: !IP
  } deriving (Show, Eq, Ord)

            
data UrlQuery = UrlQuery { url:: !ByteString, query:: !ByteString } deriving (Show, Eq, Read, Ord, Generic)

data Rule = Js     { name:: !String, path:: ![String], maxReqWithoutJs:: !Int, countPath:: ![String]}
          | Period { name:: !String, period:: !Int, countByPeriod:: !Int, countPath:: ![String]}
          | Regexp { name:: !String, regexp:: !String, urlQuery:: ![UrlQuery]}
          | Botnet { name:: !String, period:: !Int, countByPeriod:: !Int, size' :: !Int}
          deriving (Show, Eq, Read, Ord, Generic)
                                   
data BanRule = BanRule { banName:: !String, rules:: !Rules } deriving (Show, Eq, Read, Generic)

data MongoConnect = MongoConnect
data MembaseConnect = MembaseConnect

data Connect a = Connect { connectType::a, hostname::String, port::PortNumber }                                      

                       
data Antibot = Antibot { reqT:: TChan Request
                       , result:: TChan Result
                       , banMessage:: TChan Result
                       , timeState:: TMVar TM
                       , sock:: Socket
                       , websock::Socket
                       , static :: String
                       , state:: TMVar [Process]
                       , startP:: Name -> ServerSideT ()
                       , startH:: Rules -> ServerSideT ()
                       , banrules:: TMVar BanRules
                       , rulesA:: TMVar Rules
                       , banIp:: TMVar SM
                       , whiteList::TMVar [IPSubnet]
                       , membase::TMVar (Connect MembaseConnect)
                       , membaseConnect::TMVar MC.Server
                       , mongoPipe::TMVar Pipe
                       , mongo::TMVar (Connect MongoConnect)
                       } 
 
class (Functor m, Monad m, MonadIO m) => ServerReader m where
    ask :: m Antibot
    
class (ServerReader m) => Get m a where
    get::m a

class (ServerReader m) => Post m a where
    put::a -> m [a]
    rm::a -> m [a]

class (ServerReader m) => ProcInfo m b where
    register::b -> m ()
    unregister::b -> m ()



instance Show Antibot where
    show _ = "ok"
    
    
instance ServerReader Application where
    ask = Reader.ask
    
instance ServerReader ServerSideT where
    ask = Reader.ask
    
instance (ServerReader m) => Get m SM where
    get = banIp <$> ask >>= liftIO . atomically . readTMVar
    
instance (ServerReader m) => Get m TM where
    get = timeState <$> ask >>= liftIO . atomically . readTMVar
    
    
instance (ServerReader m) => Get m Banneds where
    get = do x <- banIp <$> ask >>= liftIO . atomically . readTMVar
             return $! Data.List.take 100 $ map (\y -> Banned y) $ filter (\(_,r) -> banned r) $ toList x
   
instance (ServerReader m) => Get m Rules where
    get = rulesA <$> ask >>= liftIO . atomically . readTMVar
    
    
instance (ServerReader m) => Get m BanRules where
    get = banrules <$> ask >>= liftIO  . atomically . readTMVar
    
instance (ServerReader m) => Get m [IPSubnet] where
    get = whiteList <$> ask >>= liftIO . atomically . readTMVar
    
instance (ServerReader m) => Get m [Process] where
    get = state <$> ask >>= liftIO . atomically . readTMVar
    
              
instance (ServerReader m) => Get m (Maybe (Connect MembaseConnect)) where
    get = membase <$> ask >>= liftIO . atomically . tryReadTMVar
    
instance (ServerReader m) => Get m (Maybe (Connect MongoConnect)) where
    get = mongo <$> ask >>= liftIO . atomically . tryReadTMVar
    
instance  (ServerReader m) => Get m Socket where
    get = sock <$> ask 

instance  (ServerReader m) => Get m (TChan Result) where
    get = result <$> ask

instance  (ServerReader m) => Get m (TChan Request) where
    get = reqT <$> ask 

instance  (ServerReader m) => Get m (TMVar [Process]) where
    get = state <$> ask

instance  (ServerReader m) => Get m (TMVar SM) where
    get = banIp <$> ask 
    
instance  (ServerReader m) => Get m (TMVar TM) where
    get = timeState <$> ask 
    
    
instance (ServerReader m) => Post m (Connect MembaseConnect) where
    put (Connect MembaseConnect hostname port) = do
        t <- membaseConnect <$> ask
        m <- membase <$> ask
        handle <- liftIO $ atomically $ tryTakeTMVar t
        case handle of 
             Nothing -> return ()
             Just h -> liftIO $ MC.disconnect h
        connection <- liftIO $ MC.connect hostname port   
        liftIO $ atomically $ putTMVar t connection
        liftIO $ atomically $ do
            tryTakeTMVar m
            putTMVar m $ Connect MembaseConnect hostname port
        return $! [Connect MembaseConnect hostname port]
    rm (Connect MembaseConnect _ _) = do
          t <- membaseConnect <$> ask
          m <- membase <$> ask
          handle <- liftIO $ atomically $ tryTakeTMVar t
          case handle of
               Nothing -> return ()
               Just h -> liftIO $ MC.disconnect h
          liftIO $ atomically $ tryTakeTMVar m
          return []

instance (ServerReader m) => Post m (Connect MongoConnect) where
    put (Connect MongoConnect hostname port) = do
        p <- mongoPipe <$> ask
        m <- mongo <$> ask
        handle <- liftIO $ atomically $ tryTakeTMVar p
        case handle of
             Nothing -> return ()
             Just h -> liftIO $ close h
        pipe <- liftIO $ runIOE $ connect $ Host hostname (PortNumber port )
        liftIO $ atomically $ putTMVar p pipe
        liftIO $ atomically $ do
            tryTakeTMVar m
            putTMVar m $ Connect MongoConnect hostname port
        return $! [Connect MongoConnect hostname port]
    rm (Connect MongoConnect _ _) = do
        p <- mongoPipe <$> ask
        m <- mongo <$> ask
        handle <- liftIO $ atomically $ tryTakeTMVar p
        case handle of
             Nothing -> return ()
             Just h -> liftIO $ close h
        liftIO $ atomically $ tryTakeTMVar m
        return $! []

instance (ServerReader m) => Post m IPSubnet where
    put b = do t <- whiteList <$> ask
               liftIO $ atomically $ do
                   x <- takeTMVar t 
                   putTMVar t (b:x) 
                   return (b:x)
    rm b = do t <- whiteList <$> ask
              liftIO $ atomically $ do
                  x <- takeTMVar t
                  putTMVar t (filter (/= b) x)
                  return (filter (/= b) x)
                  
instance  (ServerReader m) => Post m BanRule where
    put b = do t <- banrules <$> ask
               liftIO $ atomically $ do
                   x <- takeTMVar t 
                   putTMVar t (b:x) 
                   return (b:x)
    rm b = do t <- banrules <$> ask
              liftIO $ atomically $ do
                  x <- takeTMVar t
                  putTMVar t (filter (/= b) x)
                  return (filter (/= b) x)
                               
instance  (ServerReader m) => Post m Rule where
    put r = do t <- rulesA <$> ask
               server <- ask -- server state
               fun <- startH <$> ask -- fun for start handler
               liftIO $ runAntibot server (fun [r]) -- start handler process
               liftIO $ atomically $ do
                   x <- takeTMVar t 
                   putTMVar t (r:x) 
                   return (r:x)
    rm b = do t <- rulesA <$> ask
              ban <- banrules <$> ask
              rm (Process ("handler." ++ name b, Nothing))
              liftIO $ do
                  let fun (BanRule _ y) = b `notElem` y
                  atomically $ do
                      x <- takeTMVar ban
                      putTMVar ban (filter fun x)
                      y <- takeTMVar t
                      putTMVar t (filter (/= b) y)
                      return (filter (/= b) y)
                  
instance  (ServerReader m) => Post m Process where
    put p@(Process (n,_)) = do
        allP <- state <$> ask >>= liftIO . atomically . readTMVar
        let Process (_, x) = findProcess p allP
        case x of
             Just _ -> return allP
             Nothing -> do
                server <- ask
                if isPrefixOf "handler." n 
                    then do
                        fun <- startH <$> ask
                        r <- get
                        let toStart = filter (\y -> "handler." ++ name y == n) r
                        liftIO $ print n
                        liftIO $ runAntibot server (fun toStart) 
                    else do
                       fun <- startP <$> ask 
                       liftIO $ runAntibot server (fun n) 
                state <$> ask >>= liftIO . atomically . readTMVar
    rm p  = do
        allP <- state <$> ask >>= liftIO . atomically . readTMVar
        let Process (n, x) = findProcess p allP
        when (isJust x) $ do
            server <- ask
            liftIO $ do
                killThread $ fromJust x
                liftIO $ print $ show n
                runAntibot server (unregister p) 
        state <$> ask >>= liftIO . atomically . readTMVar
        

instance  (ServerReader m) => ProcInfo m Process where
    register p = do
        processMV <- get::ServerReader m => m (TMVar [Process])
        liftIO $ atomically $ do
            x <- takeTMVar processMV 
            putTMVar processMV (p:x)
    unregister (Process (name,_)) = do
        processMV <- get::ServerReader m => m (TMVar [Process])
        liftIO $ atomically $ do
            x <- takeTMVar processMV 
            putTMVar processMV (filter (\(Process (n, _)) -> name /= n) x)
      
instance FromJSON Rule where
    parseJSON (Object v) =  Js <$> v .: "name" <*> v .: "path" <*>  v .: "maxReqWithoutJs" <*> v .: "countPath" <* (v .: "type"::Parser Text) <|>
                            Period <$> v .: "name" <*> v .: "period" <*> v .: "countByPeriod" <*> v .: "countPath" <* (v .: "type"::Parser Text) <|>
                            Regexp <$> v .: "name" <*> checkRegexp v <*> v .: "urlQuery" <* (v .: "type"::Parser Text) <|>
                            Botnet <$> v .: "name" <*> v .: "period" <*> v .: "countByPeriod" <*> v .: "size"
    parseJSON _ = mzero
instance ToJSON Process where
    toJSON (Process (n, p)) = object [ "type" .= processt
                                     , "name" .= n
                                     , "pid"  .= show p
                                     ]
instance FromJSON Process where
    parseJSON (Object v) = toProcess <$> v .: "type" <*> v .: "name" <*> v .: "pid"
    parseJSON _ = mzero
    
instance ToJSON BanRule
instance FromJSON BanRule                                    

instance ToJSON (Connect MembaseConnect) where
    toJSON (Connect MembaseConnect hostname port) = 
        object [ "hostname" .= hostname
               , "port" .= (fromIntegral $ port::Int)
               , "type" .= membaseT
               ]
instance ToJSON (Connect MongoConnect) where
    toJSON (Connect MongoConnect hostname port) = 
        object [ "hostname" .= hostname 
               , "port" .= (fromIntegral $ port ::Int)
               , "type" .= mongoT
               ]
                      
instance FromJSON (Connect MembaseConnect) where
    parseJSON (Object v) = parseC <$> v .: "hostname" <*> v .: "port" <*> v .: "type"
                           where parseC = parseConnect MembaseConnect
    parseJSON _ = mzero
    
instance FromJSON (Connect MongoConnect) where
    parseJSON (Object v) = parseC  <$> v .: "hostname" <*> v .: "port" <*> v .: "type"
                           where parseC = parseConnect MongoConnect
    parseJSON _ = mzero
                                     
parseConnect::z -> String -> Int -> Text -> Connect z
parseConnect x s i _ = Connect x s (fromIntegral i)


instance ToJSON UrlQuery
instance FromJSON UrlQuery

instance ToJSON Rule where
    toJSON Js{..} = object [ "type" .= jst
                           , "name" .= name
                           , "path" .= path
                           , "maxReqWithoutJs" .= maxReqWithoutJs
                           , "countPath" .= countPath 
                           ]
    toJSON Period{..} = object [ "type" .= periodt
                               , "name" .= name
                               , "period" .= period
                               , "countByPeriod" .= countByPeriod
                               , "countPath" .= countPath 
                               ]
    toJSON Regexp{..} = object [ "type" .= regexpt
                               , "name" .= name
                               , "regexp" .= regexp
                               , "urlQuery" .= toJSON urlQuery 
                               ]
    toJSON Botnet{..} = object [ "type" .= botnet
                               , "name" .= name
                               , "period" .= period
                               , "countByPeriod" .= countByPeriod
                               , "size" .= size'
                               ]
                               
instance ToJSON Banned where
    toJSON (Banned (i, r)) = object [ "type" .= bannedt
                                    , "ip" .= show i
                                    , "rule" .= (map ruleToName $! ruleState r)
                                    , "count" .= countM r
                                    , "banned" .= banned r
                                    ]
                                    
instance ToJSON IP where
    toJSON x = object [ "type" .= ipT
                      , "ip" .= show x
                      ]
                      
instance FromJSON IP where
    parseJSON (Object v) = do
        x <- v .: "ip" <* (v .: "type"::Parser Text)
        case parseIp (encodeUtf8 x) of
             Nothing -> mzero
             Just a -> return a
    parseJSON _ = mzero
        
                      
instance ToJSON IPSubnet where
    toJSON x = object [ "type" .= subT
                      , "subnet" .= show x
                      ]
                      
instance FromJSON IPSubnet where
    parseJSON (Object v) = do
        x <- v .: "subnet" <* (v .: "type"::Parser Text)
        case P.parseOnly parsOr (encodeUtf8 x) of
             Right a -> return a
             Left _ -> mzero
                      
    parseJSON _ = mzero
           
instance Show Process where
    show (Process (p,n)) = p ++ show n
    

findProcess::Process -> [Process] -> Process
findProcess (Process (name, _)) l = let fun = \(Process (n, _)) -> n == name
                                    in case filter fun l of
                                            [] -> Process (name, Nothing)
                                            [x] -> x
                                            x -> error $ show x
  
clearBase::Application Response
clearBase = do
   sm <- banIp <$> ask 
   st <- timeState <$> ask
   void . liftIO . atomically $ do
       swapTMVar sm empty
       swapTMVar st empty
   return $! toResponse ("ok"::Text)

jst, regexpt, periodt, processt, bannedt, ipT, subT, membaseT, mongoT, botnet::Text
jst = "js"
regexpt = "regexp"
periodt = "period"
processt = "process"
bannedt = "banned"
ipT = "ip"
subT = "subnet"
membaseT = "membase"
mongoT = "mongo"
botnet = "botnet"

eithToMaybe::Either a b -> Maybe b
eithToMaybe (Left _) = Nothing
eithToMaybe (Right x) = Just x

ruleToName::Rule -> String
ruleToName (Js n _ _ _) = "js." ++ n
ruleToName (Period n _ _ _) = "period." ++ n
ruleToName (Regexp n _ _) = "regexp." ++ n
    
checkRegexp::Object -> Parser String
checkRegexp v = do
    x <- v .: "regexp"
    case compileM (B8.pack x) [] of
         Left _ -> mzero
         Right _ -> return x

toProcess::String -> String -> String -> Process
toProcess "process" n _ = Process (n, Nothing)
toProcess _ _ _ = undefined


tryReadMVar::MVar a -> IO (Maybe a)
tryReadMVar m = mask_ $ do
    a <- tryTakeMVar m
    case a of
       Nothing -> return Nothing
       Just a' -> do
           putMVar m a'
           return $ Just a'

parseIp::B8.ByteString -> Maybe IP
parseIp x = case P.parseOnly pars' x of
                 Right a -> Just a
                 Left _ -> Nothing
 
octet::P.Parser Int
octet = P.decimal <* P.char '.'

pars'::P.Parser IP
pars' = do
    a <- fromIntegral <$> octet
    b <- fromIntegral <$> octet
    c <- fromIntegral <$> octet
    d <- fromIntegral <$> P.decimal
    return $ IPv4 (a `shift` 24 + b `shift` 16 + c `shift` 8 + d)
    
parsSub::P.Parser IPSubnet
parsSub = do
    a <- fromIntegral <$> octet
    b <- fromIntegral <$> octet
    c <- fromIntegral <$> octet
    d <- fromIntegral <$> P.decimal
    P.char '/'
    e <- P.decimal
    case e >= 0 && e <= 32 of
         True -> return $ network (ip a b c d) (mask e)
         False -> fail "bad mask"
    where ip a b c d  = IPv4 (a `shift` 24 + b `shift` 16 + c `shift` 8 + d)
          maxMask = maxBound::Word32
          mask x = Mask32 $ shiftL (shiftR maxMask (32 - x)) (32 - x)
 
ipToSub::IP -> IPSubnet
ipToSub x = IPSubnet x $ Mask32 (maxBound::Word32)

parsOr::P.Parser IPSubnet
parsOr = choice [ parsSub
                , ipToSub <$> pars'
                ]
