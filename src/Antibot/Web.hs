{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module Antibot.Web (web) where

import Happstack.Server ( FromReqURI(..)
                        , path
                        , serveDirectory
                        , methodM
                        , askRq
                        , takeRequestBody
                        , unBody
                        , Response
                        , toResponse
                        , dir
                        , dirs
                        , ok
                        , Method( GET, POST )
                        , Browsing( EnableBrowsing )
                        , toResponseBS
                        , badRequest
                        )
import Control.Monad.Reader (liftIO, msum, mzero, ask, void)
import Data.Aeson (encode, decode, ToJSON(..), FromJSON(..), (.:), Value(..), object, (.=))
import Data.Network.Ip (IP, IPSubnet)
import Antibot.Logger (Stored, getInfoByIp)
import Data.Time (formatTime, getZonedTime, getCurrentTime, UTCTime)
import Control.Applicative ((<$>))
import Database.MongoDB (runIOE, connect, close, Host(..), PortID(PortNumber))
import System.Locale (defaultTimeLocale)
import Data.Bson.Generic (fromBSON)
import Data.Maybe (mapMaybe, fromMaybe)
import GHC.Generics (Generic)
import Data.Ord (comparing)
import Data.List (sortBy, reverse)
import Control.Concurrent.MVar (MVar, withMVar)
import Control.Concurrent.STM (TMVar, atomically, readTMVar)
import Antibot.Data ( Application
                    , Connect(..)
                    , Process
                    , MongoConnect(..)
                    , MembaseConnect
                    , Rule
                    , Rules
                    , SM
                    , TM
                    , BanRule
                    , BanRules
                    , put
                    , get
                    , Result(..)
                    , ruleState
                    , rm
                    , countM
                    , ruleToName
                    , eithToMaybe
                    , ip'
                    , parseIp
                    , clearBase
                    , static
                    )
import qualified Antibot.Data as A
import Data.ByteString.Char8 (ByteString, pack)
import Data.Map (filter, lookup, member, (!), toAscList, toDescList, toList, elems, size)
import qualified Data.ByteString.Lazy.Char8  as L (empty, ByteString)
import Prelude hiding (lookup, filter)
--import Debug.Trace

class Web m a where 
   isBannedIp::a -> m Bool
   getRequests::a -> m [Stored]
   infoByIp::a -> m IpInfo
   
instance Web Application IP where
    isBannedIp i = (get::Application (TMVar TM)) >>= \tm -> liftIO $ atomically $ do
        x <- readTMVar tm
        return $ i `member` x
    getRequests i = do
        maybeConf <- get :: Application (Maybe (Connect MongoConnect))
        case maybeConf of
             Nothing -> return []
             Just (Connect MongoConnect hostname port) -> do
                 pipe <- liftIO $ runIOE $ connect $ Host hostname (PortNumber port)
                 today <- liftIO $ formatTime defaultTimeLocale "%F" <$> getZonedTime
                 result <- liftIO $ getInfoByIp (show i) today pipe
                 liftIO $ close pipe
                 return $ mapMaybe fromBSON $ fromMaybe [] $ eithToMaybe result
    infoByIp i = do
        let getRul x y = fromMaybe [] $ ruleState <$> lookup x y
            getCount x y = fromMaybe 0 $ countM <$> lookup x y
            getTime x y t = fromMaybe t $ lookup x y
        t <- liftIO getCurrentTime
        stored <- getRequests i
        isBanned <- isBannedIp i
        if isBanned 
             then do
                 tm <- get ::Application (TMVar TM)
                 sm <- get ::Application (TMVar SM)
                 (rule, cont) <- liftIO $ atomically $ readTMVar sm >>= \x -> return   (getRul i x, getCount i x)
                 time         <- liftIO $ atomically $ readTMVar tm >>= \x -> return $ getTime i x t
                 return  $ IpInfo i True rule time cont stored
             else return $ IpInfo i False [] t 0 stored
                 
data IpInfo = IpInfo { ip::IP
                     , banned::Bool
                     , rules::Rules
                     , bannedTime::UTCTime
                     , count::Int
                     , requests::[Stored]
                     } deriving  (Show, Generic)
                     
type Page = Int

data Sort = IpSort | RuleSort | TimeSort | CountSort deriving (Eq, Generic)

instance Show Sort where
    show IpSort = "ip"
    show RuleSort = "rule"
    show TimeSort = "time"
    show CountSort = "count"

data Banned = Banned { ipB::IP
                     , rulesB::Rules
                     , countB::Int
                     , timeB::UTCTime
                     } deriving (Show, Eq)
                     
data Paginator = Paginator Sort Bool Page deriving (Show, Eq, Generic)

instance ToJSON Paginator where
    toJSON (Paginator s b p) = object [ "sort" .= show s
                                      , "asc"  .= b
                                      , "page" .= p
                                      ]
                                      
instance FromJSON Paginator where
    parseJSON (Object v) = do
        s <- parseSort <$> v .: "sort"
        a <- v .: "asc"
        p <- v .: "page"
        case s of
             Nothing -> mzero
             Just so -> return $ Paginator so a p 
        
        

parseSort::String -> Maybe Sort
parseSort "ip" = Just IpSort
parseSort "rule" = Just RuleSort
parseSort "time" = Just TimeSort
parseSort "count" = Just CountSort
parseSort _ = Nothing
    
                     
instance ToJSON Banned where
    toJSON Banned{..}  = object [ "ip" .= show ipB 
                                , "rules" .= map ruleToName rulesB 
                                , "count" .= countB 
                                , "time" .= timeB 
                                ]
    
                     
type Banneds = [Banned]

getSize::Application Response
getSize = do
    tm <- get::Application TM
    return $ toResponse $ show $ truncate $ (fromIntegral (size tm)) / 500
    

getBanned::Paginator -> Application Banneds
getBanned (Paginator IpSort b p) = do
    !sm <- get::Application SM
    !tm <- get::Application TM
    let sorted = if b 
                    then toAscList tm
                    else toDescList tm
        ips = getP p sorted
        getR i = ruleState $ sm ! i
        getC i = countM $ sm ! i
        fun (i, t) = Banned i (getR i) (getC i) t
    return $ map fun ips
getBanned (Paginator RuleSort b p) = do
    !sm <- get::Application SM
    let sortFun = comparing ruleState 
        onlyBanned = elems $ filter (\x -> A.banned x == True) sm
        sorted = if b 
                     then sortBy sortFun onlyBanned
                     else reverse $  sortBy sortFun onlyBanned
        ips = getP p sorted
        fun Result{..} = Banned ip' ruleState countM timeM
    return $ map fun ips
getBanned (Paginator CountSort b p) = do
    !sm <- get::Application SM
    let sortFun = comparing countM 
        onlyBanned = elems $ filter (\x -> A.banned x == True) sm
        sorted = if b 
                     then sortBy sortFun onlyBanned
                     else reverse $ sortBy sortFun onlyBanned
        ips = getP p sorted
        fun Result{..} = Banned ip' ruleState countM timeM
    return $ map fun ips
getBanned (Paginator TimeSort b p) = do
    !sm <- get::Application SM
    !tm <- get::Application TM
    let getTime = snd
    let sortFun = comparing getTime 
        sorted = if b 
                     then sortBy sortFun $ toList tm
                     else reverse $ sortBy sortFun $ toList tm
        ips = getP p sorted
        getI = fst
        getR (i,_) = ruleState $ sm ! i
        getC (i,_) = countM $ sm ! i
        getT = snd
        fun x = Banned (getI x) (getR x) (getC x) (getT x)
    return $ map fun ips
   
getP::Int -> [a] -> [a]
getP x ys = take 500 $ drop (x*500) ys
    
    
                     
instance ToJSON Stored
instance ToJSON IpInfo

instance FromReqURI ByteString where
      fromReqURI s = Just $ pack s

web :: Application Response
web = msum [ dir "rules"        $ getHandler  (get::Application Rules)
           , dir "rules"        $ postHandler (put::Rule -> Application Rules)
           , dirs "rules/rm"    $ postHandler (rm::Rule -> Application Rules)
           , dir "banrules"     $ getHandler  (get::Application BanRules)
           , dir "banrules"     $ postHandler (put::BanRule -> Application BanRules)
           , dirs "banrules/rm" $ postHandler (rm::BanRule -> Application BanRules)
           , dir "process"      $ getHandler  (get::Application [Process])
           , dir "process"      $ postHandler (restart::Process -> Application [Process])
           , dir "banned"       $ postHandler getBanned
           , dir "banned"       $ path $ \x -> toAppResponse (parseIp x) infoByIp
           , dir "white"        $ getHandler  (get::Application [IPSubnet])
           , dir "white"        $ postHandler (put::IPSubnet -> Application [IPSubnet])
           , dirs "white/rm"    $ postHandler (rm::IPSubnet -> Application [IPSubnet])
           , dir "membase"      $ getHandler  (get::Application (Maybe (Connect MembaseConnect)))
           , dir "membase"      $ postHandler (put::(Connect MembaseConnect) -> Application [Connect MembaseConnect])
           , dir "mongo"        $ postHandler (put::(Connect MongoConnect) -> Application [Connect MongoConnect])
           , dir "mongo"        $ getHandler  (get::Application (Maybe (Connect MongoConnect)))
           , dir "clearbase"     $ clearBase
           , dir "size"         $  getSize
           , staticHandler
           ]


staticHandler::Application Response
staticHandler = do
    path <- static <$> ask
    serveDirectory EnableBrowsing ["app/index.html"] path
       
getHandler::ToJSON a => Application a -> Application Response
getHandler a = methodM GET >> a >>= ok . toJsonResponse

postHandler::(ToJSON b, FromJSON a) => (a -> Application b) -> Application Response
postHandler a = methodM POST >> getBody >>= \x -> toAppResponse (decode x) a

toJsonResponse ::ToJSON a => a -> Response                                                                 
toJsonResponse x = toResponseBS (pack "application/json") $ encode x

toAppResponse::ToJSON b => Maybe a -> (a -> Application b) -> Application Response
toAppResponse Nothing _ = bad
toAppResponse (Just x) fun = do res <- fun x
                                ok $ toJsonResponse res
                                
bad::Application Response
bad = badRequest $ toResponse ("alarm !!!"::String)

getBody :: Application L.ByteString
getBody = do
    req'  <- askRq          
    body' <- liftIO $ takeRequestBody req'  
    case body' of          
         Just rqbody -> return . unBody $ rqbody 
         Nothing     -> return L.empty 
           
restart::Process -> Application [Process]
restart p = do
    void $ rm p
    liftIO $ print "remove"
    liftIO $ print p
    put p

