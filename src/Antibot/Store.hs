{-# LANGUAGE RecordWildCards #-}
module Antibot.Store (Store(..), storeT, closeMembaseConnect) where

import Control.Concurrent.STM (TMVar, takeTMVar, TChan, atomically, readTMVar, tryReadTMVar, tryTakeTMVar, putTMVar, readTChan)
import Data.Network.Ip (IPSubnet, isHostInNetwork)
import Control.Monad.Reader (ReaderT, forever, when, ask, liftIO, unless)
import Control.Applicative ((<$>))
import Control.Exception (try, SomeException)
import Data.Maybe (isJust, fromJust)
import Control.Concurrent.MVar (MVar, modifyMVar_, withMVar, tryTakeMVar, putMVar)
import Antibot.Data (Connect(..), tryReadMVar, MembaseConnect(..), Result(..), TM, eithToMaybe, ruleToName)
import Data.Map (insert)
import Network.Memcache (setttl)
import Network.Memcache.Protocol (Server, connect, sIsConnected, disconnect)
-- import Debug.Trace

data Store = Store { membaseSetting:: TMVar (Connect MembaseConnect)
                   , membase' :: TMVar Server
                   , input :: TChan Result
                   , white :: TMVar [IPSubnet]
                   , timeMap :: TMVar TM
                   }

type StoreT a = ReaderT Store a


storeT::StoreT IO ()
storeT = forever $ do
    banned <- getResult
    white' <- isWhite banned
    ts <- getTM
    unless white' $! do
        liftIO $ atomically $ do
            x <- takeTMVar ts
            putTMVar ts (insert (ip' banned) (timeM banned) x)
        writeToMembase banned
    

getResult::StoreT IO Result
getResult = input <$> ask >>= liftIO . atomically . readTChan

isWhite::Result -> StoreT IO Bool
isWhite Result{..} = do
    m <- white <$> ask
    liftIO $! atomically $ readTMVar m >>= \x -> return $ any (\y -> ip' `isHostInNetwork` y) x
    
getTM::StoreT IO (TMVar TM)
getTM = timeMap <$> ask

writeToMembase::Result -> StoreT IO ()
writeToMembase x = do
    connect <- getMembaseConnect
    when (isJust connect) $ do
       let rul =  foldr1 (\a b -> a ++ ',' : b) $ map ruleToName $ ruleState x
           i = show $ ip' x
       liftIO $ try (setttl (fromJust connect) i rul 86400)::StoreT IO (Either SomeException Bool) 
       return ()
            
getMembaseConnect::StoreT IO (Maybe Server)
getMembaseConnect = do
    connect <- membase' <$> ask >>= liftIO . atomically . tryReadTMVar
    case connect of
         Just x -> reconnectMembase $ Just x
         Nothing -> startMembase                    
        
startMembase::StoreT IO (Maybe Server)
startMembase = do
    conf <- membaseSetting <$> ask 
    connectT <- membase' <$> ask
    maybeConf <- liftIO $ atomically $ tryReadTMVar conf
    maybeConnect <- liftIO $ makeConnect maybeConf
    when (isJust maybeConnect) $ liftIO $ atomically $ do
        tryTakeTMVar connectT
        putTMVar connectT $ fromJust maybeConnect
    return maybeConnect
             
         
makeConnect::Maybe (Connect MembaseConnect) -> IO (Maybe Server)
makeConnect Nothing = return Nothing 
makeConnect (Just (Connect MembaseConnect hostname port)) = do
    x <- try (connect hostname port)::IO (Either SomeException Server)
    return $ eithToMaybe x
                 
reconnectMembase::Maybe Server -> StoreT IO (Maybe Server)
reconnectMembase Nothing = return Nothing
reconnectMembase (Just connect) = do
    open <- liftIO $ sIsConnected connect
    if open 
       then return $ Just connect
       else startMembase
                 

closeMembaseConnect::StoreT IO ()
closeMembaseConnect = do
    maybeMembase <- membase' <$> ask >>= liftIO . atomically . tryReadTMVar
    case maybeMembase of
         Nothing -> return ()
         Just x -> liftIO $ do  
                                try (disconnect x) ::IO (Either SomeException ())
                                return ()
 
