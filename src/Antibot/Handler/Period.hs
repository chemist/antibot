{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE BangPatterns #-}
module Antibot.Handler.Period (perioder, Counter(..)) where

import Control.Monad (void, when, forever)
import Control.Monad.State (StateT, get, put)
import Control.Monad.IO.Class (liftIO)
import Control.Concurrent (threadDelay, forkIO)
import Control.Concurrent.STM (TChan, TMVar, atomically, readTMVar, swapTMVar, isEmptyTChan, readTChan, writeTChan)
import Antibot.Data (Request(..), Rule(..), Result(..))
import Data.Network.Ip (IP)
import Data.ByteString.Char8 (ByteString)
import Data.Map (Map, insertLookupWithKey, empty)
import qualified Data.Set as Set (empty)
import Data.Set (Set, member, notMember, insert)

--------------------------------------------------------------------------------------------------
-- period
--------------------------------------------------------------------------------------------------
    
data Counter = Counter { input::TChan Request        -- ^ get request
                       , output::TChan Result -- ^ write result for ban
                       , lock::TMVar Bool        -- ^ lock 
                       , rule:: !Rule              -- ^ rule
                       , bann::Set IP          -- ^ banned ip
                       , counter::Map IP Int   -- ^ count request
                       , countPathSet::Set ByteString
                       }  

type PeriodState a = StateT Counter a

-- | get time period from state
timeP::PeriodState IO Int
timeP = do
    Counter{..} <- get
    return $ period rule
    
-- | get lock status from state 
-- | True -> count result and write to chan
-- | False -> count request
lockP::PeriodState IO Bool
lockP = do
    Counter{..} <- get
    liftIO $ atomically $ readTMVar lock
    
-- | write False to lock TTMVar
disableLockP::PeriodState IO (TMVar Bool)
disableLockP = do
    Counter{..} <- get
    void . liftIO $ atomically $ swapTMVar lock False
    return lock

-- | just count request
periodP::PeriodState IO ()
periodP = do
    c@Counter{..} <- get
    !b <- liftIO $ atomically $ isEmptyTChan input
    when b $ liftIO $ threadDelay 100000
    !Request{..} <- liftIO $ atomically $ readTChan input
    let fun _  = (+)
        (p, _) = rRequest
        isCountPathOk = member p countPathSet
        (count, m) = insertLookupWithKey fun rIp (toN isCountPathOk) counter
        isNotBanned = notMember rIp bann
    put $ c { counter = m }
    case count of
          Nothing -> return ()
          Just i -> when (i > countByPeriod rule) $ when isNotBanned $ do
              put $ c { bann = insert rIp bann, counter = m }
              liftIO $ void . atomically $ writeTChan output $ Result rTime [rule] i rIp False
                    
toN::Bool -> Int
toN True = 1
toN False = 0

-- | when lock, rewrite Map with counts, disable lock, count and send result to chan
perioder::PeriodState IO ()
perioder = forever $ do
    l <- lockP
    if l 
       then do c@Counter{..} <- get
               put $! c { bann = Set.empty, counter = empty }
               lM <- disableLockP
               liftIO $ forkIO $ pause lM (period rule)
               return ()
       else periodP
           
-- | pause, after write True to lock
pause::TMVar Bool -> Int -> IO ()
pause lock time = do
    threadDelay (1000000 * time)
    atomically $ swapTMVar lock True
    return ()

--------------------------------------------------------------------------------------------------
-- end  period
--------------------------------------------------------------------------------------------------
