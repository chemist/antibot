{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
module Antibot.Handler.BotNet where

import Antibot.Data hiding (get, put)
import Data.Network.Ip
import Control.Monad.Reader
import Control.Concurrent.STM
import Control.Concurrent
import Data.Set
import Data.Foldable
import Data.Time


data Boter = Boter 
  { input :: (TChan Request) 
  , output:: (TChan Result) 
  , rule  :: !Rule 
  , network :: !(Set IP)
  , requests :: TMVar Integer
  , mass :: TMVar Bool
  }
  
type BoterST a = ReaderT Boter a

killBotNet :: BoterST IO ()
killBotNet = backRequest >> counter

counter :: BoterST IO ()
counter = forever $ do
    Boter{..} <- ask  
    req <- liftIO $ atomically $ readTChan input
    when (member (rIp req) network) $ do
        liftIO $ addRequest requests
        r <- liftIO $ atomically $ readTMVar requests 
        when (r >= (toInteger $ countByPeriod rule)) $ banNet req
    return ()
    
backRequest :: BoterST IO ()
backRequest = do
    Boter{..} <- ask
    let count = countByPeriod rule
        time  = period rule
        v = fromIntegral count / toRational time
    void . liftIO $ forkIO $ do
        threadDelay $ 60 * 1000000
        atomically $ do
            x <- takeTMVar requests
            let succ' x = (fromIntegral x) - 60 * v
            putTMVar requests $ toInteger $ truncate $ if (succ' x < 0) then 0 else succ' x
    

addRequest :: TMVar Integer -> IO ()
addRequest x = atomically $ takeTMVar x >>= \y -> putTMVar x (y + 1)

banNet :: Request -> BoterST IO ()
banNet req = do
    Boter{..} <- ask
    mass' <- liftIO $ atomically $ readTMVar mass
    time <- liftIO $ getCurrentTime
    when mass' $ do
        traverse_ (sendToBan time rule output) network
        liftIO $ atomically $ swapTMVar mass False
        return ()
    liftIO $ atomically $ writeTChan output $ Result time [rule] 1 (rIp req) False
    where 
    sendToBan time rule output x = liftIO $ atomically $ writeTChan output $ Result time [rule] 1 x False
              
    



