{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE BangPatterns #-}
module Antibot.Handler.Regex (regexper, Regexper(..)) where

import Control.Monad (forever, when, void, join)
import Text.Regex.PCRE.Light (Regex, match)
import Control.Concurrent.STM (TChan, readTChan, atomically, writeTChan)
import Antibot.Data (Request(..), Rule(..), Result(..), UrlQuery(..))
import Control.Monad.IO.Class (liftIO)
import Data.Maybe (isJust, fromMaybe)
import Data.ByteString.Char8 (empty)
import Data.Set (Set, member)
import Control.Monad.State (StateT, get)
import Network.HTTP.Types

data Regexper = Regexper { input::TChan Request
                         , output::TChan Result
                         , rule:: Rule
                         , re:: Regex
                         , pathSet::Set UrlQuery
                         }
                         
type RegexperState a = StateT Regexper a


regexper::RegexperState IO ()
regexper = forever $ do
    !Regexper{..} <- get
    !Request{..} <- liftIO $ atomically $ readTChan input
    let (p, q) = rRequest
        isPathOk = or $! map (\x -> member  (UrlQuery p (fst x)) pathSet) q
        str = map (\x -> fromMaybe empty $ join $ lookup (query x) q) $ urlQuery $ rule
        result = length $! filter isJust $ map (\x -> match re x []) str
    when (isPathOk && result > 0) $ liftIO $ void . atomically $ writeTChan output $ Result rTime [rule] 1 rIp False

