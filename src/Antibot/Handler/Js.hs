{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
module Antibot.Handler.Js (jser, Jser(..)) where

import Control.Concurrent.STM (TMVar, atomically, writeTChan, readTChan, TChan, readTMVar, swapTMVar)
import Antibot.Data (Request(..), Result(..), Rule, maxReqWithoutJs )
import Control.Monad.State (StateT, get)
import Data.Map (Map, insertLookupWithKey, insert, filter)
import Control.Monad (forever, when, void)
import Control.Monad.IO.Class (liftIO)
import Data.Maybe (fromMaybe)
import Data.Time (UTCTime, getCurrentTime, diffUTCTime)
import Control.Concurrent (forkIO, threadDelay)
import Data.Network.Ip (IP)
import Data.ByteString.Char8 (ByteString)
import Prelude hiding (filter)
import Data.Set (Set, member)
import System.Posix.Syslog (syslog, Priority(Notice))
import Network.HTTP.Types
-- import Debug.Trace

data Count = Count { time:: !UTCTime, i:: !Int }

(>+<) ::  Count -> Count -> Count
(>+<) a b = Count (time b) (i a + i b)

data Jser = Jser { input::TChan Request
                 , output::TChan Result
                 , rule:: !Rule
                 , requests::TMVar (Map IP Count)
                 , pathSet::Set ByteString
                 , countPathSet::Set ByteString
                 }
                         
type JsState a = StateT Jser a

jser::JsState IO ()
jser = do
    Jser{..} <- get
    liftIO $ forkIO $ forever $ do
        cleaner requests
        threadDelay $ 1800 * 1000000
    jser'
        
jser'::JsState IO ()
jser' = forever $ do
    Jser{..} <- get
    r@Request{..} <- liftIO $ atomically $ readTChan input
    liftIO $ syslog Notice $ show r
    let (p, _) = rRequest
        isPathOk = member  p pathSet
        isCountPathOk = member p countPathSet
    result <- if isCountPathOk || isPathOk then addRequest isPathOk rIp else do
        liftIO . void . atomically $ writeTChan output $ CleanJS rIp
        return 0
    when (result > maxReqWithoutJs rule) $ liftIO $ void . atomically $ writeTChan output $ Result rTime [rule] 1 rIp False
    return ()

addRequest::Bool -> IP -> JsState IO Int
addRequest False ip = do
    Jser{..} <- get
    now <- liftIO getCurrentTime
    count <- liftIO $ atomically $ do
        var <- readTMVar requests
        let fun _ = (>+<)
            (count', newmap) = insertLookupWithKey fun ip (Count now 1) var
        swapTMVar requests newmap
        return count'
    return $ i $ fromMaybe (Count now 1) count
addRequest True ip = do
    Jser{..} <- get
    now <- liftIO getCurrentTime
    liftIO $  atomically $ do
        var <- readTMVar requests
        swapTMVar requests $ insert ip (Count now 0) var
    return 1
    
cleaner::TMVar (Map IP Count) -> IO ()
cleaner m = do
    now <- getCurrentTime
    atomically $ do
        var <- readTMVar m
        let fun x = 1800 > diffUTCTime now (time x)
            newRequests = filter fun var
        swapTMVar m newRequests 
    return ()
