{-# LANGUAGE NoMonomorphismRestriction, FlexibleContexts, RankNTypes,KindSignatures #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE OverloadedStrings #-}
module Antibot.Input (input) where

import Antibot.Data (Request(..), parseIp)
import Data.Conduit (runResourceT, Source, ($=), (=$=),($$),Conduit, Sink)
import Data.Conduit.Util ()
import Data.Conduit.List (map, mapMaybe, mapM_)
import Data.Conduit.Network.UDP (sourceSocket, Message(msgData))
import Network.Socket (Socket)
import Prelude hiding (map, take, concat, mapM_)
import qualified Prelude as P
import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.Serialize (decode)
import Control.Monad (liftM)
import Data.Time (UTCTime, minutesToTimeZone, LocalTime(..), TimeOfDay(..), fromGregorian, localTimeToUTC)
import Control.Concurrent (forkIO) 
import Control.Concurrent.STM (TChan, atomically, writeTChan)
import Network.HTTP.Types.URI (urlDecode)
import Data.Attoparsec (Parser)
import Control.Applicative ((<$>), (<*))
import Data.Fixed (Pico, resolution)
import Data.Attoparsec.ByteString.Char8 (parseOnly, decimal, char, space, take, anyChar)
import Data.ByteString.Char8 (split, splitWith, concat)
import Data.ByteString (ByteString)


input::Socket -> TChan Request -> IO ()
input isock chan = let udp = inputSource isock
                       toChan = sinkChan chan
                   in runResourceT $! udp $= messageToBs =$= bsToRequest $$ toChan
                   
inputSource::MonadIO m => Socket -> Source m Message
inputSource isock = sourceSocket isock 3268

messageToBs::MonadIO m => Conduit Message m ByteString
messageToBs = map msgData

bsToRequest::MonadIO m => Conduit ByteString m Request
bsToRequest = mapMaybe binToRequest


binToRequest::ByteString -> Maybe Request
binToRequest x = case decode x of
                    Right a -> Just a
                    Left _ -> Nothing
                    
sinkChan::MonadIO m => TChan Request -> Sink Request m ()
sinkChan chan = mapM_ fun
  where 
  fun x = liftIO . atomically $! writeTChan chan x

