{-# LANGUAGE OverloadedStrings, ExtendedDefaultRules #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DeriveDataTypeable     #-}
{-# LANGUAGE BangPatterns #-}
module Antibot.Logger (Stored, getInfoByIp, Logger(..), logger, closeMongo) where

import Antibot.Data (Request(..), TM, Connect(..), MongoConnect(..), tryReadMVar)
import Control.Concurrent.STM (TChan, TMVar, atomically, readTChan, tryReadTMVar, tryTakeTMVar, putTMVar)
import Database.MongoDB ( Action
                        , Document
                        , Pipe
                        , Failure(ConnectionFailure)
                        , Collection
                        , AccessMode( UnconfirmedWrites )
                        , select
                        , limit
                        , access
                        , master
                        , rest
                        , allCollections
                        , find
                        , insert_
                        , Database
                        , runIOE
                        , connect
                        , PortID(PortNumber)
                        , Host(..)
                        , close
                        )
import Control.Applicative ((<$>))
import Control.Monad.Trans (liftIO)
import Control.Monad (forever, when)
import Data.Network.Ip (IP)
import Data.ByteString.Char8 (ByteString)
import Data.Text.Encoding (decodeUtf8, decodeUtf8')
import Data.Time (formatTime, getZonedTime, UTCTime)
import System.Locale (defaultTimeLocale)
import Control.Exception (catch, try, SomeException)
import Data.Bson.Generic (ToBSON(..), FromBSON(..))
import Data.Bson ((=:))
import GHC.Generics (Generic)
import Data.Typeable (Typeable)
import Data.Maybe (isNothing, isJust, fromJust)
import Prelude hiding (catch)
import System.Posix.Syslog (syslog, Priority(Notice))
import Control.Concurrent.MVar (MVar, withMVar, tryTakeMVar, putMVar)
import Control.Monad.Reader (ask, ReaderT)
import Data.Map (member)
import Data.Text (pack, Text)
import Control.Concurrent.STM
-- import Debug.Trace


findIP ::String -> String -> Action IO [Document]
findIP i c = let ip = pack i
                 col = pack c
                 iip = pack "ip_"
                 time' = pack "time"
             in rest =<< find (select [iip =: ip] col) { limit = 200 }
             
db::Text
db = pack "banned"

getCollections::Pipe -> IO (Either Failure [Collection])
getCollections pipe = access pipe master db allCollections

getInfoByIp::String -> String -> Pipe -> IO (Either Failure [Document])
getInfoByIp i c pipe = access pipe master db (findIP i c)
               

data Logger = Logger { input::TChan Request
                     , banIP::TMVar TM
                     , mongoC::TMVar (Connect MongoConnect)
                     , pipe::TMVar Pipe
                     } 
                     
type LoggerT a = ReaderT Logger a
        
getReq::LoggerT IO Request
getReq = do
    t <- input <$> ask
    liftIO $! atomically $ readTChan t

isBanned::IP -> LoggerT IO Bool
isBanned i = do
    ban' <- banIP <$> ask 
    liftIO $! atomically $ readTMVar ban' >>= \x -> return $ i `member` x
    
toBin::Request -> Action IO ()
toBin req = do
    today <- liftIO $ formatTime defaultTimeLocale "%F" <$> getZonedTime
    insert_ (pack today) (bson req)

bson::Request -> Document
bson Request{..} = toBSON $! Stored  (pack $ show rIp) rTime (pack $ show rStatusCode) (decodeWithFail rHttpRefer) (decodeWithFail rUserAgent) (pack $ show rRequest)


data Stored = Stored { ip_:: !Text
                     , time:: !UTCTime
                     , status:: !Text
                     , refer:: !Text
                     , agent :: !Text
                     , request:: !Text
                     } deriving (Generic, Typeable, Show, Eq)
                     
instance ToBSON Stored
instance FromBSON Stored
               
decodeWithFail::ByteString -> Text
decodeWithFail x = case decodeUtf8' x of
                        Left _ -> pack "not unicode"
                        Right y -> y
               
slash::Text
slash = "/"

logger::LoggerT IO ()
logger = forever $ do
    pipeT <- pipe <$> ask
    maybePipe <- liftIO $ atomically $ tryReadTMVar pipeT
    when (isNothing maybePipe) startMongo
    req <- getReq
    check <- isBanned (rIp req)
    when (check && isJust maybePipe) $ do 
        !result <- liftIO $ catch (access (fromJust maybePipe) UnconfirmedWrites bannedDB (toBin req))
                                (\e -> do 
                                          syslog Notice $ "problem with mongodb" ++ show e
                                          return $ Left $ ConnectionFailure e)
        case result of
             Left e -> do liftIO $ syslog Notice $ "problem in logger " ++ show e
                          closeMongo
                          return ()
             _ -> return ()
  
bannedDB::Database
bannedDB = "banned"

startMongo::LoggerT IO ()
startMongo = do
    conf <- mongoC <$> ask
    pipeT <- pipe <$> ask
    maybeConf <- liftIO $ atomically $ tryReadTMVar conf
    when (isJust maybeConf) $ do
        let Connect MongoConnect hostname port = fromJust maybeConf
        p <- liftIO $ try (runIOE $ connect $ Host hostname (PortNumber port))::LoggerT IO (Either SomeException Pipe)
        case p of
            Left e -> {- (liftIO $ syslog Notice "cant connection to mongodb") >> -} return ()
            Right x -> liftIO $ do
                 syslog Notice "have connection to mongodb"
                 atomically $ do
                     tryTakeTMVar pipeT
                     putTMVar pipeT x
    return ()
             
closeMongo::LoggerT IO ()
closeMongo = do
    liftIO $ syslog Notice "close mongodb connection"
    pipeT <- pipe <$> ask
    maybePipe <- liftIO $ atomically $ tryTakeTMVar pipeT
    when (isJust maybePipe) $ liftIO $ do
        try (close (fromJust maybePipe))::IO(Either SomeException ())
        return ()

