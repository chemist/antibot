{-# LANGUAGE BangPatterns #-}
module Antibot.Ban (Ban(..), banE) where
import Antibot.Data (Rule(Js), Result(..), BanRules, BanRule(rules), Rules, SM)
import Control.Concurrent.STM (TChan, takeTMVar, putTMVar, TMVar, atomically, readTChan, writeTChan, readTMVar)
import Control.Monad (forever, when)
import Control.Monad.Reader (ReaderT, ask, liftIO)
import Control.Applicative ((<$>))
import Data.Map ((!), update, member, insert, insertWith, lookup)
import Data.Network.Ip (IP)
import Data.List (nub, (\\))
import Control.Concurrent.MVar (MVar, modifyMVar, withMVar, readMVar)
import Prelude hiding (lookup)

data Ban = Ban { input::TChan Result
               , output::TChan Result
               , banrules'::TMVar BanRules
               , banIp::TMVar SM
               }
               
type BanT a = ReaderT Ban a


banE::BanT IO ()
banE = forever $ do
    inputT <- getResult
    case inputT of
         CleanJS{} -> removeJSRules (ip' inputT)
         _ -> do
             outputT <- getOutput
             baninputT <- getBanRules
             banT <- getBanIp
             let ip = ip' inputT
             !isNew <- liftIO $ atomically $ do
                 x <- takeTMVar banT
                 let newBanIp = makeNewMap inputT baninputT x 
                 putTMVar banT newBanIp
                 return $! isNewState ip x newBanIp
             when isNew $ do
                 liftIO $ atomically $ do
                     x <- readTMVar banT 
                     writeTChan outputT (x ! ip)
                     
removeJSRules :: IP -> BanT IO ()
removeJSRules i = getBanIp >>= removeJS
  where
  removeJS sm = liftIO $ atomically $ do
      sm' <- takeTMVar sm
      putTMVar sm $ update fun i sm'
  fun r = Just $ r { ruleState = filter fil $ ruleState r} 
  fil Js{} = False
  fil _ = True
        
makeNew::BanRules -> Result -> Result -> Result
makeNew x new old = new { timeM = timeM new
                        , ruleState = nub $ ruleState new ++ ruleState old 
                        , banned = ban (nub $ ruleState new ++ ruleState old) x
                        , countM = countM old
                        }
                           
ifFistBan::BanRules -> Result -> Result
ifFistBan x new  = new { banned = ban (ruleState new) x }
                            
makeNewMap::Result -> BanRules -> SM -> SM
makeNewMap x br sm = if ip' x `member` sm
                          then insertWith (makeNew br) (ip' x) x sm
                          else insert (ip' x) (ifFistBan br x) sm      

getResult::BanT IO Result 
getResult = input <$> ask >>= liftIO . atomically . readTChan 

getBanIp::BanT IO (TMVar SM)
getBanIp = banIp <$> ask

getOutput::BanT IO (TChan Result)
getOutput = output <$> ask

getBanRules::BanT IO BanRules
getBanRules = banrules' <$> ask >>= liftIO . atomically . readTMVar
    
    
isNewState::IP -> SM -> SM -> Bool
isNewState i banMap banMap' = if member i banMap 
                                   then (banned <$> lookup i banMap) /= (banned <$> lookup i banMap')
                                   else (banned <$> lookup i banMap') == Just True

ban::Rules -> BanRules -> Bool
ban rule brules = any (\x -> [] == x \\ rule) $ map rules brules

