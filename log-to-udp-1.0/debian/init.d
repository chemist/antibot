#!/bin/sh

DAEMON=/opt/log-to-udp/log-to-udp


case "$1" in
    start)
		echo start
        $DAEMON server -u www-data -g www-data -p /var/run/log-to-udp/ -c /etc/log-to-udp.conf --start
         ;;
    stop)
        $DAEMON server -u www-data -g www-data -p /var/run/log-to-udp/ -c /etc/log-to-udp.conf --stop
        ;;
    *)
        echo "Usage: /etc/init.d/log-to-udp start|stop"
        exit 1
        ;;
        
esac
exit 0
